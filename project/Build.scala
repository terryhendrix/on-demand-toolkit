import sbt._

object Build extends Build {

  object V {
    val peanuts = "1.2.0"
    val akkaV = "2.3.9"
    val streamV = "1.0"
  }

  object Projects {
    lazy val peanuts = RootProject(uri("https://bitbucket.org/terryhendrix/peanuts.git#"+V.peanuts))
  }
}
