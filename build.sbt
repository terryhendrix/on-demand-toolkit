import Build.V
import Build.Projects

lazy val myProject = (project in file("."))
  .dependsOn(Projects.peanuts)
  .settings(
    organization := "nl.bluesoft",
    name := "on-demand-toolkit",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.11.6",
    resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository",
    resolvers += "krasserm at bintray" at "http://dl.bintray.com/krasserm/maven",
    resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/maven-releases/",
    credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),
    libraryDependencies ++= {
      Seq(
        "org.scala-lang"           % "scala-library"                        % scalaVersion.value,
        "com.typesafe.akka"       %% "akka-kernel"                          % V.akkaV,
        "com.typesafe.akka"       %% "akka-actor"                           % V.akkaV,
        "com.typesafe.akka"       %% "akka-slf4j"                           % V.akkaV,
        "com.typesafe.slick"      %% "slick-extensions"                     % "3.0.0",
        "com.typesafe.slick"      %% "slick"                                % "3.0.0",
        "com.typesafe.akka"       %% "akka-stream-experimental"             % V.streamV,
        "com.typesafe.akka"       %% "akka-http-core-experimental"          % V.streamV,
        "com.typesafe.akka"       %% "akka-http-experimental"               % V.streamV,
        "com.typesafe.akka"       %% "akka-http-spray-json-experimental"    % V.streamV,
        "com.typesafe.akka"       %% "akka-http-xml-experimental"           % V.streamV,
        "org.scalatest"           %% "scalatest"                            % "2.1.4" % "test",
        "com.typesafe.akka"       %% "akka-testkit"                         % V.akkaV   % "test",
        "org.pegdown"              % "pegdown"                              % "1.4.2" % "test",
        "com.typesafe.akka"       %% "akka-http-testkit-experimental"       % V.streamV % "test",
        "com.typesafe.akka"       %% "akka-stream-testkit-experimental"     % V.streamV % "test"
      )
    },
    autoCompilerPlugins := true,
    javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint"),
    scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8"),
    scalacOptions in (Compile,doc) ++= Seq("-groups", "-implicits"),
    publishMavenStyle := true,
    publishArtifact in Test := false,
    fork := true,
    fork in run := true,
    fork in test := true,
    (parallelExecution in test) := false,
    testOptions in ThisBuild += Tests.Argument(TestFrameworks.ScalaTest, "-h", "target/test-reports")
  )


