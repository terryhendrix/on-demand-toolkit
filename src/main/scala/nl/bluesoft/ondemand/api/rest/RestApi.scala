package nl.bluesoft.ondemand.api.rest
import java.text.SimpleDateFormat

import akka.actor.{ActorRefFactory, ActorSystem}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.stream.{ActorMaterializer, ActorMaterializerSettings, Materializer}
import com.typesafe.config.Config
import nl.bluesoft.ondemand.api.rest.RestApi.Responses.Message
import nl.bluesoft.ondemand.api.rest.RestApi.{Requests, RestApiJsonProtocol}
import nl.bluesoft.ondemand.api.{ApiFacade, ApiModule}
import nl.bluesoft.ondemand.common.representation.RepresentationTypes._
import nl.bluesoft.ondemand.common.representation.{SessionRepr, DemandRepr, DemandUserRepr, SupplyUserRepr}
import nl.bluesoft.peanuts.http.ExtendableRestApi
import nl.bluesoft.peanuts.http.routes.ViewRoute
import nl.bluesoft.peanuts.http.util.JsonErrorResponse
import nl.bluesoft.peanuts.json.{JsonMarshalling, PeanutsJsonProtocol}

import scala.concurrent.ExecutionContext

object RestApi
{
  object Requests
  {
    case class CreateUser(username:Username, password:Password, userType:UserType)
    case class Login(username:Username, password:Password, userType:UserType)
  }

  object Responses
  {
    case class Message(message:String)
  }

  trait RestApiJsonProtocol extends PeanutsJsonProtocol
  {
    override def jsonDateFormat: SimpleDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss")

    implicit val messageFormat = jsonFormat1(Responses.Message)

    implicit val createUserFormat = jsonFormat3(Requests.CreateUser)
    implicit val loginFormat = jsonFormat3(Requests.Login)

    implicit val sessionReprFormat = jsonFormat2(SessionRepr)
    implicit val supplyUserReprFormat = jsonFormat1(SupplyUserRepr)
    implicit val demandUserReprFormat = jsonFormat1(DemandUserRepr)
    implicit val demandReprFormat = jsonFormat8(DemandRepr)
  }
}

class RestApi(override val system:ActorSystem,
              api:ApiFacade) extends ExtendableRestApi with JsonMarshalling with JsonErrorResponse with SprayJsonSupport
                             with RestApiJsonProtocol with ViewRoute
{
  override def config: Config = system.settings.config.getConfig(ApiModule.ConfigKeys.PublicRest)
  override implicit val ec: ExecutionContext = system.dispatcher
  implicit val actorRefFactory:ActorRefFactory = system
  override implicit val mat: Materializer = ActorMaterializer(ActorMaterializerSettings(system))

  override val extendedRoute: Route = {
    pathPrefix("api" / "v1") {
      path("user") {
        post {
          entity(as[Requests.CreateUser]) { request ⇒
            complete {
              api.createUser("0.0.0.0", request.username, request.password, request.userType).map { _ ⇒
                StatusCodes.Created → Message("User created").asJson
              }
            }
          }
        }
      } ~
      path("login") {
        post {
          entity(as[Requests.Login]) { request ⇒
            complete {
              api.loginUser("0.0.0.0", request.username, request.password, request.userType).map { session ⇒
                StatusCodes.OK → session.asJson
              }.recover {
                case ex:ApiModule.FalseLoginException ⇒ StatusCodes.Unauthorized → ex.asInstanceOf[Throwable].asJson
                case ex ⇒ throw ex
              }
            }
          }
        }
      } ~
      complete(StatusCodes.NotFound → Message("Request not valid. Please check your url, method and body."))
    }
  }
}
