package nl.bluesoft.ondemand.api.system.request

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.Timeout
import nl.bluesoft.ondemand.api.{SessionAuthentication, SessionManagerAuthentication}
import nl.bluesoft.ondemand.common.ops.{ConfigOps, LoggingAdapterOps}
import nl.bluesoft.ondemand.common.representation.RepresentationTypes._
import nl.bluesoft.ondemand.demand.DemandFacade
import nl.bluesoft.ondemand.user.UserModule

import scala.concurrent.ExecutionContext

object ServiceReplyWorker {
  def props(sessionManager:ActorRef, demand:DemandFacade) = Props(new ServiceReplyWorkerImpl(sessionManager, demand))

  case class RespondToServiceRequest(sessionID:SessionID, demand:DemandID, acknowledged:Boolean)
}

trait ServiceReplyWorker extends Actor with ActorLogging with SessionAuthentication with LoggingAdapterOps
{
  implicit def ec:ExecutionContext
  def demand:DemandFacade

  override def receive:Receive = {
    case ServiceReplyWorker.RespondToServiceRequest(sessionID, demandRepr, acknowledged) ⇒
      authenticate(sessionID).map { session ⇒
        demand.respondToServiceRequest(session.username, demandRepr, acknowledged)
      }.recover { case ex ⇒
        log.logException("authenticating while responding to service request", ex)
      }
    case unhandled ⇒
      log.logUnhandledMessage(unhandled)
  }
}

class ServiceReplyWorkerImpl(override val sessionManager:ActorRef, override val demand:DemandFacade) extends ServiceReplyWorker with SessionManagerAuthentication with ConfigOps
{
  override implicit val timeout: Timeout = context.system.settings.config.getTimeout(UserModule.ConfigKeys.GenericTimeout)
  override implicit val ec:ExecutionContext = context.dispatcher
}



