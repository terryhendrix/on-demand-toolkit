package nl.bluesoft.ondemand.api.system.request

import akka.actor._
import akka.pattern._
import nl.bluesoft.ondemand.api.ApiModule
import nl.bluesoft.ondemand.api.system.session.SessionManager
import nl.bluesoft.ondemand.common.ops.ConfigOps
import nl.bluesoft.ondemand.common.representation.{SessionRepr, RepresentationTypes}
import RepresentationTypes._
import nl.bluesoft.ondemand.user.UserFacade

object LoginWorker {
  def props(userModule:UserFacade, sessionManager:ActorRef) = Props(new LoginWorkerImpl(userModule,sessionManager))

  case class Login(username:Username, password:Password, userType: UserType)
  case class FalseLoginAttempt(username:Username)
  case class Authenticated(username:Username, session:SessionRepr)
}

trait LoginWorker extends Actor with ActorLogging with ConfigOps
{
  def userModule:UserFacade
  def sessionManager:ActorRef

  context.setReceiveTimeout(context.system.settings.config.getFiniteDuration(ApiModule.ConfigKeys.RequestTimeout))

  def login(username:Username, password:Password, userType: UserType):Unit

  override def receive: Receive = {
    case LoginWorker.Login(username, password, userType) ⇒
      login(username, password, userType)

    case ReceiveTimeout ⇒
      self ! PoisonPill
  }
}

class LoginWorkerImpl(override val userModule:UserFacade, override val sessionManager:ActorRef) extends LoginWorker
{
  implicit lazy val timeout = context.system.settings.config.getTimeout(ApiModule.ConfigKeys.RequestTimeout)
  implicit lazy val ec = context.dispatcher

  override def login(username: Username, password: Password, userType: UserType): Unit = {
    val ref = sender()
    userModule.loginUser(username, password, userType).map {
      case true  ⇒
        sessionManager ask SessionManager.CreateSession(username) map {
          case SessionManager.CreatedSession(session) ⇒
            ref ! LoginWorker.Authenticated(username, session)
            self ! PoisonPill
          case SessionManager.SessionAlreadyExists(session) ⇒
            log.error("Could not create a new session due duplicate session key. Attempt logging in again.")
            self ! LoginWorker.Login(username, password, userType)
        }
      case false ⇒
        ref ! LoginWorker.FalseLoginAttempt(username)
        self ! PoisonPill
    }
  }
}

