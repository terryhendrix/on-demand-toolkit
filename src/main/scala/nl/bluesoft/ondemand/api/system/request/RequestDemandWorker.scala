package nl.bluesoft.ondemand.api.system.request

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.Timeout
import nl.bluesoft.ondemand.api.{SessionAuthentication, SessionManagerAuthentication}
import nl.bluesoft.ondemand.common.ops.{ConfigOps, LoggingAdapterOps}
import nl.bluesoft.ondemand.common.representation.DemandRepr
import nl.bluesoft.ondemand.common.representation.RepresentationTypes._
import nl.bluesoft.ondemand.demand.DemandFacade
import nl.bluesoft.ondemand.user.UserModule

import scala.concurrent.ExecutionContext

object RequestDemandWorker {
  def props(sessionManager:ActorRef, demand:DemandFacade) = Props(new RequestDemandWorkerImpl(sessionManager, demand))

  case class RequestDemand(ip: SourceIpAddress, demand: DemandRepr, session: SessionID)
}

trait RequestDemandWorker extends Actor with ActorLogging with SessionAuthentication with LoggingAdapterOps
{
  implicit def ec:ExecutionContext
  def demand:DemandFacade

  override def receive:Receive = {
    case RequestDemandWorker.RequestDemand(ip, demandRepr, sessionID) ⇒
      authenticate(sessionID).map { session ⇒
        demand.requestDemand(demandRepr)
      }.recover { case ex ⇒
        log.logException("authenticating while requesting demand", ex)
      }
    case unhandled ⇒
      log.logUnhandledMessage(unhandled)
  }
}

class RequestDemandWorkerImpl(override val sessionManager:ActorRef, override val demand:DemandFacade) extends RequestDemandWorker with SessionManagerAuthentication with ConfigOps
{
  override implicit val timeout: Timeout = context.system.settings.config.getTimeout(UserModule.ConfigKeys.GenericTimeout)
  override implicit val ec:ExecutionContext = context.dispatcher
}



