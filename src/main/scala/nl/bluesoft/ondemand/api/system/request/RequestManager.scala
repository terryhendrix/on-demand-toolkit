package nl.bluesoft.ondemand.api.system.request

import akka.actor.{Actor, ActorLogging, Props}
import nl.bluesoft.ondemand.api.{MockedRequestManagement, RequestManagement}
import nl.bluesoft.ondemand.common.ops.LoggingAdapterOps
import nl.bluesoft.ondemand.common.representation.RepresentationTypes
import RepresentationTypes.{SourceIpAddress, Username}

object RequestManager {
  def props = Props(new RequestManagerImpl)

  case class HandleRequest[U <: AnyRef](ipAddress:SourceIpAddress, request:U, username:Option[Username] = None)
  case class Register[U <: AnyRef](request:Class[U], props:Props)
}

trait RequestManager extends Actor with ActorLogging with RequestManagement with LoggingAdapterOps
{
  var registered:Map[Class[_ <: AnyRef], Props] = Map.empty

  override def receive: Receive = {
    case RequestManager.Register(request, props) ⇒
      registered = registered + (request → props)
      log.info(s"Registered worker properties for ${request.getSimpleName}")

    case RequestManager.HandleRequest(ip, request, username) ⇒
      if(isBlacklisted(ip)) {
        log.warning(s"Received request from blacklisted ip $ip.")
      }
      else {
        val possibleProps = registered.get(request.getClass)

        possibleProps.foreach { props ⇒
          log.info(s"Handling ${request.getClass.getSimpleName} from $ip ${username.map(u ⇒ s" for user $u").getOrElse("")}")
          context.actorOf(props, createRequestId(request.getClass)) forward request
        }

        possibleProps.getOrElse {
          log.warning(s"Not handling ${request.getClass.getSimpleName}, there are no worker properties registered.")
        }
      }

    case msg ⇒
      log.logUnhandledMessage(msg, logMessage = "Try wrapping your request like RequestManager.HandleRequest(ip, username, request)")
  }
}

class RequestManagerImpl extends RequestManager with MockedRequestManagement {

}
