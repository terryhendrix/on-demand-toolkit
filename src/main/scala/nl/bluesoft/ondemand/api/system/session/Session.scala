package nl.bluesoft.ondemand.api.system.session

import akka.actor.{ActorLogging, Props}
import nl.bluesoft.ondemand.api.SessionManagement
import nl.bluesoft.ondemand.api.system.session.Session.State
import nl.bluesoft.ondemand.common.representation.{SessionRepr, RepresentableActor, RepresentationTypes}
import RepresentationTypes.{SessionID, Username}

private[session] object Session {
  def props = Props(new SessionImpl)
  case class CreateSession(username:Username, session:SessionID)

  case class State(username:Username, session:SessionID)
}

/**
 * A session represents a validated, authenticated connection with an end user.
 */
trait Session extends RepresentableActor[Session.State, SessionRepr] with ActorLogging with SessionManagement
{
  override def receive:Receive = uninitialized

  var session:Session.State = _

  override def representationOf(state: State): SessionRepr = SessionRepr(state.username, state.session)

  /**
   * The `Session` starts in `uninitialized` state.
   * When receiving `CreateSession` it will create the session for the username that is in the message.
   * It will then transit to `initialized`
   * @return
   */
  def uninitialized: Receive = {
    case Session.CreateSession(user, sessionID) ⇒
      createSession(user, sessionID)
      sender ! SessionManager.CreatedSession(representationOf(session))
      context.become(initialized)

    case SessionManager.ValidateSession(sessionID) ⇒
      sender ! SessionManager.InvalidSession(sessionID)
  }

  /**
   * The `Session` transits to this state when it has received a `CreateSession`  message.
   * It will now respond to the `CreateSession` message with `SessionAlreadyExists`.
   * @return
   */
  def initialized: Receive = {
    case Session.CreateSession(user, sessionID) ⇒
      log.warning(s"Attempt to create a session for user $user that already exists for user ${session.username}.")
      sender ! SessionManager.SessionAlreadyExists(sessionID)

    case SessionManager.ValidateSession(sessionID) if validateSession(sessionID) ⇒
      sender ! SessionManager.ValidatedSession(representationOf(session))

    case SessionManager.ValidateSession(sessionID) ⇒
      sender ! SessionManager.InvalidSession(sessionID)
  }
}

class SessionImpl extends Session
{
  override def createSession(username: Username, sessionID: SessionID): Unit = {
    session = Session.State(username, sessionID)
    log.info(s"Session $sessionID created for $username.")
  }

  override def validateSession(sessionID: SessionID): Boolean = {
    sessionID == session.session
  }
}

