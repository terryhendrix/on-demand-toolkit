package nl.bluesoft.ondemand.api.system.request

import akka.actor._
import nl.bluesoft.ondemand.api.ApiModule
import nl.bluesoft.ondemand.common.ops.ConfigOps
import nl.bluesoft.ondemand.common.representation.RepresentationTypes
import RepresentationTypes._
import nl.bluesoft.ondemand.user.UserFacade

object CreateUserWorker {
  def props(userModule:UserFacade) = Props(new CreateUserWorkerImpl(userModule))

  case class CreateUser(username:Username, password:Password, userType: UserType)
  case class UserCreated(username:Username)
  case class UserAlreadyExists(username:Username)
}

trait CreateUserWorker extends Actor with ActorLogging with ConfigOps
{
  def userModule:UserFacade
  implicit lazy val ec = context.dispatcher

  context.setReceiveTimeout(context.system.settings.config.getFiniteDuration(ApiModule.ConfigKeys.RequestTimeout))

  override def receive: Receive = {
    case CreateUserWorker.CreateUser(username, password, userType) ⇒
      val ref = sender()
      userModule.registerUser(username, password, userType).map {
        case true  ⇒ ref ! CreateUserWorker.UserCreated(username)
        case false ⇒ ref ! CreateUserWorker.UserAlreadyExists(username)
      }

    case ReceiveTimeout ⇒
      self ! PoisonPill
  }
}

class CreateUserWorkerImpl(override val userModule:UserFacade) extends CreateUserWorker
