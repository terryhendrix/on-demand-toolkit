package nl.bluesoft.ondemand.api.system.session

import akka.actor.{Actor, ActorLogging, Props}
import nl.bluesoft.ondemand.api.{ApiModule, SessionManagement}
import nl.bluesoft.ondemand.common.ops.{LoggingAdapterOps, ConfigOps}
import nl.bluesoft.ondemand.common.representation.{SessionRepr, RepresentationTypes}
import RepresentationTypes.{SessionID, Username}

import scala.util.Try

object SessionManager {
  def props = Props(new SessionManagerImpl)
  case class CreateSession(username:Username)
  case class ValidateSession(session:SessionID)
  case class ValidatedSession(session:SessionRepr)
  case class InvalidSession(session:SessionID)
  case class CreatedSession(session:SessionRepr)
  case class SessionAlreadyExists(sessionID: SessionID)
}

private[session] trait SessionManager extends Actor with ActorLogging with SessionManagement
{
  override def receive:Receive = {
    case SessionManager.CreateSession(username) ⇒
      createSession(username, createSessionIdentifier)

    case SessionManager.ValidateSession(session) ⇒
      validateSession(session)
  }
}

private[session] class SessionManagerImpl extends SessionManager with LoggingAdapterOps with ConfigOps
{
  implicit val ec = context.dispatcher
  implicit val timeout = context.system.settings.config.getTimeout(ApiModule.ConfigKeys.GenericTimeout)

  override def createSession(username:String, sessionID:SessionID) = {
    Try {
      log.info(s"Creating session for $username.")
      context.actorOf(Session.props, sessionID) forward Session.CreateSession(username, sessionID)
    }.recover { case ex ⇒
      log.logException("creating session", ex)
    }
  }

  override def validateSession(sessionID: SessionID): Boolean = {
    context.child(sessionID) match {
      case Some(ref) ⇒
        ref forward SessionManager.ValidateSession(sessionID)
        true
      case None ⇒
        log.logSecurityViolation("Attempt to validate a non existing sessionID.")
        false
    }
  }
}
