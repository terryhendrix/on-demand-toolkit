package nl.bluesoft.ondemand.api
import akka.actor.{ActorRef, ActorSystem, Props}
import akka.event.Logging
import akka.pattern._
import nl.bluesoft.ondemand.api.ApiModule.FalseLoginException
import nl.bluesoft.ondemand.api.rest.RestApi
import nl.bluesoft.ondemand.api.system.request._
import nl.bluesoft.ondemand.api.system.session.SessionManager
import nl.bluesoft.ondemand.common.ops.{ConfigOps, LoggingAdapterOps}
import nl.bluesoft.ondemand.common.representation.RepresentationTypes._
import nl.bluesoft.ondemand.common.representation.{DemandRepr, SessionRepr}
import nl.bluesoft.ondemand.demand.{DemandFacade, DemandModule}
import nl.bluesoft.ondemand.user.{UserFacade, UserModule}

import scala.concurrent.Future

trait ApiFacade
{
  protected def registerRequestHandler(clazz:Class[_ <: AnyRef], props:Props):Unit
  
  def createUser(ip:SourceIpAddress, username:Username, password:Password, userType: UserType):Future[Unit]

  def loginUser(ip:SourceIpAddress, username:Username, password:Password, userType: UserType):Future[SessionRepr]

  def requestDemand(ip:SourceIpAddress, demand:DemandRepr)(implicit session:SessionID):Future[Unit]

  def sessionManager:ActorRef

  /**
   * Supply users respond to the demand requests. The user is identified by the session.
   * @param ip              The `SourceIpAddress` of the end user.
   * @param demand          The `DemandRepr` to respond to.
   * @param session         The session is used for user identification.
   * @return
   */
  def respondToDemand(ip:SourceIpAddress, demand:DemandID, acknowledged:Boolean)(implicit session:SessionID):Future[Unit]
}

object ApiModule
{
  class FalseLoginException(message:String) extends Exception(message)

  def apply(system:ActorSystem):ApiFacade = {
    val users = UserModule(system)
    val demand = DemandModule(system, users)
    val module = new ApiModule(system, users, demand)
    module.preStart()
    module
  }

  object ConfigKeys {
    val Base = "api"
    val RequestTimeout = s"$Base.request-timeout"
    val GenericTimeout = s"$Base.timeout"
    val PublicRest = s"$Base.public"
  }
}

class ApiModule(system:ActorSystem, users:UserFacade, demand: DemandFacade) extends ApiFacade with ConfigOps with LoggingAdapterOps
{
  val log = Logging(system, "API")
  implicit val timeout = system.settings.config.getTimeout(ApiModule.ConfigKeys.RequestTimeout)
  implicit val ec = system.dispatcher
  lazy val rest = new RestApi(system, this)

  override val sessionManager = system.actorOf(SessionManager.props, "SessionManager")
  val requestManager = system.actorOf(RequestManager.props, "RequestManager")

  def preStart() = {
    registerRequestHandler(classOf[CreateUserWorker.CreateUser], CreateUserWorker.props(users))
    registerRequestHandler(classOf[LoginWorker.Login], LoginWorker.props(users, sessionManager))
    registerRequestHandler(classOf[RequestDemandWorker.RequestDemand], RequestDemandWorker.props(sessionManager, demand))
    registerRequestHandler(classOf[ServiceReplyWorker.RespondToServiceRequest], ServiceReplyWorker.props(sessionManager, demand))
    rest.initializeRestApi(system)
    log.info("Started ApiModule")
  }

  override protected def registerRequestHandler(clazz:Class[_ <: AnyRef], props:Props) = {
    log.info(s"Registering ${clazz.getSimpleName} with the request manager")
    requestManager ! RequestManager.Register(clazz, props)
  }

  override def createUser(ip:SourceIpAddress, username: Username, password: Password, userType: UserType): Future[Unit] = {
    requestManager ask RequestManager.HandleRequest(ip, CreateUserWorker.CreateUser(username, password, userType)) map {
      case CreateUserWorker.UserAlreadyExists(us) ⇒ throw new RuntimeException(s"Username $us is already taken. Please choose another.")
      case CreateUserWorker.UserCreated(us) ⇒ Unit
    }
  }

  override def loginUser(ip:SourceIpAddress, username: Username, password: Password, userType: UserType): Future[SessionRepr] = {
    requestManager ask RequestManager.HandleRequest(ip, LoginWorker.Login(username, password, userType)) map {
      case LoginWorker.FalseLoginAttempt(us) ⇒ throw new FalseLoginException(s"Invalid credentials.")
      case LoginWorker.Authenticated(us, session) ⇒ session
    }
  }

  override def requestDemand(ip: SourceIpAddress, demand: DemandRepr)(implicit session: SessionID): Future[Unit] = {
    requestManager ask RequestManager.HandleRequest(ip, RequestDemandWorker.RequestDemand(ip, demand, session)) map {
      case LoginWorker.FalseLoginAttempt(us) ⇒ throw new RuntimeException(s"Username $us is already taken. Please choose another.")
      case LoginWorker.Authenticated(us, session) ⇒ session
    }
  }

  override def respondToDemand(ip: SourceIpAddress, demand: DemandID, acknowledged:Boolean)(implicit session: SessionID): Future[Unit] = {
    requestManager ask RequestManager.HandleRequest(ip, ServiceReplyWorker.RespondToServiceRequest(session, demand, acknowledged)) map {
      case LoginWorker.FalseLoginAttempt(us) ⇒ throw new RuntimeException(s"Username $us is already taken. Please choose another.")
      case LoginWorker.Authenticated(us, session) ⇒ session
    }
  }
}
