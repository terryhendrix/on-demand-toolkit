package nl.bluesoft.ondemand.api
import java.util.UUID
import nl.bluesoft.ondemand.common.representation.RepresentationTypes
import RepresentationTypes.{Username, SessionID}

trait SessionManagement
{
  def createSession(username:Username, sessionID:SessionID):Unit

  def validateSession(sessionID:SessionID):Boolean

  def createSessionIdentifier = UUID.randomUUID().toString
}
