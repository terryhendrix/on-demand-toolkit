package nl.bluesoft.ondemand.api
import akka.actor.ActorRef
import akka.event.LoggingAdapter
import akka.pattern._
import akka.util.Timeout
import nl.bluesoft.ondemand.api.system.session.SessionManager
import nl.bluesoft.ondemand.common.ops.LoggingAdapterOps
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.SessionID
import nl.bluesoft.ondemand.common.representation.SessionRepr

import scala.concurrent.{ExecutionContext, Future}

trait SessionAuthentication
{
  def authenticate(session:SessionID):Future[SessionRepr]
}

trait SessionManagerAuthentication extends LoggingAdapterOps {
  this:SessionAuthentication ⇒

  def log:LoggingAdapter

  implicit def ec:ExecutionContext

  implicit def timeout:Timeout

  def sessionManager:ActorRef

  def authenticate(session:SessionID):Future[SessionRepr] = {
    sessionManager ? SessionManager.ValidateSession(session) map {
      case SessionManager.ValidatedSession(sessionRepr) ⇒
        sessionRepr

      case SessionManager.InvalidSession(sessionID) if session == sessionID ⇒
        throw new Exception(s"The session $sessionID is invalid")

      case unhandled ⇒
        log.logUnhandledMessage(unhandled)
        throw new Exception("There was an unhandled message.")
    }
  }
}
