package nl.bluesoft.ondemand.api
import java.util.UUID
import nl.bluesoft.ondemand.common.representation.RepresentationTypes
import RepresentationTypes.SourceIpAddress
import nl.bluesoft.ondemand.common.annotation.mocked

/**
 * An interface to manage requests.
 */
trait RequestManagement
{
  /**
   * Checks if the `ip` is blacklisted. Returns `true` if the IP is blacklisted.
   * @param ip    The IP that is blacklisted.
   */
  def isBlacklisted(ip:SourceIpAddress):Boolean

  def createRequestId(clazz:Class[_ <: AnyRef]):String
}

/**
 * Mock request management.
 */
@mocked(message = "The request management functionality is not yet fully implemented")
trait MockedRequestManagement {
  this:RequestManagement ⇒

  @mocked(message = "blacklist functionality is not yet implemented")
  override def isBlacklisted(ip:SourceIpAddress) = {
    false
  }

  override def createRequestId(clazz:Class[_ <: AnyRef]):String = s"${clazz.getSimpleName}-${UUID.randomUUID().toString}"
}
