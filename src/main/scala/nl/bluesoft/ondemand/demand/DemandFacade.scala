package nl.bluesoft.ondemand.demand
import akka.actor.{ActorRef, ActorSystem}
import akka.event.Logging
import nl.bluesoft.ondemand.common.representation.DemandRepr
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.{DemandID, Username}
import nl.bluesoft.ondemand.demand.system.DemandManager
import nl.bluesoft.ondemand.user.UserFacade
import nl.bluesoft.ondemand.user.system.UserManager

trait DemandFacade
{
  def demandManager:ActorRef
  /**
   * Requests a demand.
   * @param demand      The demand.
   */
  def requestDemand(demand:DemandRepr):Unit

  /**
   * Requests a service
   * @param username    The supplier.
   * @param demand      The demand identifier.
   */
  def respondToServiceRequest(username:Username, demand:DemandID, acknowleged:Boolean):Unit
}

object DemandModule {
  object ConfigKeys {
    val GenericTimeout = "demand.timeout"
    val ResponseTimeout = "demand.response-timeout"
  }

  def apply(system:ActorSystem, users:UserFacade):DemandFacade = {
    val module = new DemandModule(system, users)
    module.preStart()
    module
  }
}

private[demand] class DemandModule(system:ActorSystem, users:UserFacade) extends DemandFacade
{
  val log = Logging(system, "Demand")

  def preStart() = {
    log.info("Started DemandModule")
  }

  override val demandManager = system.actorOf(DemandManager.props("default"), "DemandManager-default")

  override def requestDemand(demand:DemandRepr):Unit = {
    users.userManager ! UserManager.RequestDemand(demand)
  }

  override def respondToServiceRequest(username:Username, demand:DemandID, acknowleged:Boolean):Unit = {
    users.userManager ! UserManager.RespondToServiceRequest(demand, username, acknowleged)
  }
}
