package nl.bluesoft.ondemand.demand
import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.pattern.ask
import akka.util.Timeout
import nl.bluesoft.ondemand.common.ops.LoggingAdapterOps
import nl.bluesoft.ondemand.common.representation.DemandRepr
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.DemandID
import nl.bluesoft.ondemand.demand.system.DemandRequest
import nl.bluesoft.ondemand.demand.system.DemandRequest.GetState

import scala.concurrent.{ExecutionContext, Future}

trait DemandRequestManagement extends LoggingAdapterOps with ActorLogging
{ this:Actor ⇒

  implicit def ec:ExecutionContext
  implicit def timeout:Timeout

  def demandRequest(id:DemandID):ActorRef = {
    context.child(DemandRequest.name(id)) match {
      case None       ⇒ context.actorOf(DemandRequest.props(id), DemandRequest.name(id))
      case Some(ref)  ⇒ ref
    }
  }
  
  def getDemandRepr(demandID:DemandID):Future[DemandRepr] = {
    demandRequest(demandID) ask GetState map {
      case d:DemandRepr ⇒ d
      case unhandled ⇒
        log.logUnhandledMessage(unhandled)
        throw new RuntimeException("There was an unhandled message.")
    }
  }

  def withDemandRepr(demandID:DemandID)(func: DemandRepr ⇒ Unit):Unit = getDemandRepr(demandID).map(func)
}
