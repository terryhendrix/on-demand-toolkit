package nl.bluesoft.ondemand.demand

import akka.actor.Actor
import akka.event.LoggingAdapter
import nl.bluesoft.ondemand.common.event.{DemandRequestCompleted, DemandRequestFailed, ServiceRequestRetracted, ServiceRequested}
import nl.bluesoft.ondemand.common.representation.{DemandRepr, SupplyUserRepr}

trait DemandHandling
{
  def requestService(demand:DemandRepr, supplier:SupplyUserRepr):Unit

  def retractServiceRequest(demand:DemandRepr, supplier:SupplyUserRepr):Unit

  def completeDemandRequest(demand:DemandRepr, supplier:SupplyUserRepr):Unit

  def failDemandRequest(demand:DemandRepr, reason:String):Unit
}

trait EventStreamDemandHandling
{ this:DemandHandling with Actor ⇒

  def log:LoggingAdapter

  def requestService(demand:DemandRepr, supplier:SupplyUserRepr):Unit = {
    log.info(s"Requesting service from ${supplier.username}")
    context.system.eventStream.publish(ServiceRequested(demand, supplier))
  }

  def retractServiceRequest(demand:DemandRepr, supplier:SupplyUserRepr):Unit = {
    log.info(s"Retracting service request from ${supplier.username}")
    context.system.eventStream.publish(ServiceRequestRetracted(demand, supplier))
  }

  def completeDemandRequest(demand:DemandRepr, supplier:SupplyUserRepr):Unit = {
    log.info(s"Completing demand request")
    context.system.eventStream.publish(DemandRequestCompleted(demand, supplier))
  }

  def failDemandRequest(demand:DemandRepr, reason:String):Unit = {
    log.error(s"Demand request failed: $reason")
    context.system.eventStream.publish(DemandRequestFailed(demand, reason))
  }
}