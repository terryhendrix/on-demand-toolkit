package nl.bluesoft.ondemand.demand

import akka.event.LoggingAdapter
import nl.bluesoft.ondemand.common.annotation.mocked
import nl.bluesoft.ondemand.common.representation.{DemandRepr, DemandUserRepr, SupplyUserRepr}

/**
 * Abstract Methods for supplier resolving.
 */
trait SupplierResolving
{
  def suppliers:Vector[SupplyUserRepr]
  def addSupplier(supplier:SupplyUserRepr):Unit
  def removeSupplier(supplier:SupplyUserRepr):Unit
  def nextSupplier(demand:DemandRepr, demander:DemandUserRepr, attemptedSuppliers:Seq[SupplyUserRepr]):Option[SupplyUserRepr]
}

/**
 * Mocked supplier resolving, will return all suppliers in list until it no longer has any unused suppliers.
 */
@mocked(message = "The supplier resolving is really basic. It checks the already attempted suppliers against the available suppliers. There is no sorting of any kind.")
trait MockedSupplierResolving
{
  this:SupplierResolving ⇒

  def log:LoggingAdapter
  var suppliers = Vector.empty[SupplyUserRepr]

  def addSupplier(supplier:SupplyUserRepr) = {
    log.info(s"Adding ${supplier.username} to suppliers.")
    suppliers = suppliers :+ supplier
  }

  def removeSupplier(supplier:SupplyUserRepr) = {
    log.info(s"Removing ${supplier.username} from suppliers.")
    suppliers = suppliers.filter(_.username != supplier.username)
  }

  def nextSupplier(demand:DemandRepr, demander:DemandUserRepr, attemptedSuppliers:Seq[SupplyUserRepr]):Option[SupplyUserRepr] = {
    suppliers.find(s ⇒ !attemptedSuppliers.contains(s))
  }
}
