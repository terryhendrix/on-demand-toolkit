package nl.bluesoft.ondemand.demand
import nl.bluesoft.ondemand.common.representation.DemandRepr

trait DemandRegistration
{
  def registerDemand(demand:DemandRepr):Unit
}
