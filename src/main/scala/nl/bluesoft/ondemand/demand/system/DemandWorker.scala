package nl.bluesoft.ondemand.demand.system

import akka.actor.{Actor, ActorLogging, Props, ReceiveTimeout}
import nl.bluesoft.ondemand.common.event.ServiceResponseReceived
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.DemandID
import nl.bluesoft.ondemand.common.representation.{DemandRepr, SupplyUserRepr}
import nl.bluesoft.ondemand.demand.{DemandHandling, DemandRegistration, EventStreamDemandHandling}

import scala.concurrent.duration._
import scala.language.postfixOps

object DemandWorker
{
  def props(demand:DemandRepr):Props = Props(new DemandWorkerImpl(demand))
  def name(demand:DemandID):String = s"DemandWorker-$demand"

  case object HandleDemand
  case class HandleSupplier(supplier:SupplyUserRepr)
  case object NoMoreSuppliers
}

trait DemandWorker extends DemandHandling with DemandRegistration with Actor with ActorLogging
{
  def demand:DemandRepr

  var attemptedSuppliers:Vector[SupplyUserRepr] = Vector.empty

  context.setReceiveTimeout(Duration(context.system.settings.config.getString("demand.response-timeout")))

  override def receive: Receive = register

  def register:Receive = {
    case DemandWorker.HandleDemand ⇒
      registerDemand(demand)
      context.become(handling)
      context.parent ! DemandManager.NextSupplier(demand, demand.demander)
  }

  def handling:Receive = {
    case DemandWorker.NoMoreSuppliers ⇒
      failDemandRequest(demand, "There are no more suppliers available.")

    case DemandWorker.HandleDemand ⇒
      log.warning("Already handling command, will not react to the received HandleDemand message.")

    case event:ServiceResponseReceived if event.acknowledged ⇒
      completeDemandRequest(demand, event.supplier)
      context.become(completed)

    case event:ServiceResponseReceived if !event.acknowledged ⇒
      context.parent ! DemandManager.NextSupplier(demand, demand.demander, attemptedSuppliers)

    case DemandWorker.HandleSupplier(supplier) ⇒
      attemptedSuppliers = attemptedSuppliers :+ supplier
      requestService(demand, supplier)

    case ReceiveTimeout ⇒
      retractServiceRequest(demand, attemptedSuppliers.last)
      context.parent ! DemandManager.NextSupplier(demand, demand.demander, attemptedSuppliers)
  }
  
  def completed:Receive = {
    case event:ServiceResponseReceived ⇒
      log.warning("Service response received but the request has already been completed.")
  }
}

class DemandWorkerImpl(override val demand:DemandRepr) extends DemandWorker with EventStreamDemandHandling {
  override def registerDemand(demand: DemandRepr): Unit = {
    context.parent ! DemandManager.CreateDemand(demand)
  }
}

