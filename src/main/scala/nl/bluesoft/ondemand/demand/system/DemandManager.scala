package nl.bluesoft.ondemand.demand.system

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.Timeout
import nl.bluesoft.ondemand.common.event.{DemandRequested, ServiceResponseReceived, SupplyUserLoggedIn, SupplyUserLoggedOut}
import nl.bluesoft.ondemand.common.ops.ConfigOps
import nl.bluesoft.ondemand.common.representation.RepresentationTypes._
import nl.bluesoft.ondemand.common.representation.{DemandRepr, DemandUserRepr, SupplyUserRepr}
import nl.bluesoft.ondemand.demand.{DemandModule, DemandRequestManagement, MockedSupplierResolving, SupplierResolving}

object DemandManager
{
  def props(groupId:DemandGroupID) = Props(new DemandManagerImpl(groupId))
  case class NextSupplier(demand:DemandRepr, demander:DemandUserRepr, attemptedSuppliers:Seq[SupplyUserRepr] = Seq.empty)
  case object GetSuppliers

  case class CreateDemand(demand:DemandRepr)
  case class GetDemandRepr(demand:DemandID)
}

/**
 * The demand manager manages demand for a specific service. The service is identified by the `groupId` parameters.
 * On boot it subscribes itself to the event stream. It does this for `DemandRequested`, `SupplyUserLoggedIn` and `SupplyUserLoggedOut`.
 * On termination it un-subscribes itself.
 * One must override `spawnWorker` to make the `DemandManager` actually spawn workers.
 */
trait DemandManager extends Actor with ConfigOps with ActorLogging with SupplierResolving with DemandRequestManagement
{
  implicit val ec = context.dispatcher
  implicit val timeout:Timeout = context.system.settings.config.getTimeout(DemandModule.ConfigKeys.GenericTimeout)

  def groupId:DemandGroupID

  override def preStart() = {
    log.info("Subscribing for events")
    context.system.eventStream.subscribe(self, classOf[DemandRequested])
    context.system.eventStream.subscribe(self, classOf[ServiceResponseReceived])
    context.system.eventStream.subscribe(self, classOf[SupplyUserLoggedIn])
    context.system.eventStream.subscribe(self, classOf[SupplyUserLoggedOut])
  }

  override def postStop() = {
    log.info("Unsubscribing")
    context.system.eventStream.unsubscribe(self)
  }

  override def receive: Receive = {
    case DemandManager.CreateDemand(demand) ⇒
      log.info("Storing demand request.")
      demandRequest(demand.id) ! DemandRequest.Create(demand)

    case DemandManager.GetDemandRepr(id) ⇒
      demandRequest(id) forward DemandRequest.GetState

    case DemandManager.GetSuppliers ⇒
      sender ! suppliers

    case event:DemandRequested ⇒
      log.info("Handling requested demand")
      worker(event.demand) forward DemandWorker.HandleDemand

    case event:ServiceResponseReceived ⇒
      if(event.acknowledged)
        demandRequest(event.demand) ! DemandRequest.SupplierAcknowledged(event.supplier)

      log.info(s"Routing response for demand ${event.demand}")
      withDemandRepr(event.demand) { demand ⇒
        worker(demand) ! event
      }

    case DemandManager.NextSupplier(demand, demander, attemptedSuppliers) ⇒
      val ref = sender()
      nextSupplier(demand, demander, attemptedSuppliers) match {
        case Some(supplier) ⇒
          demandRequest(demand.id) ! DemandRequest.SupplierAttempted(supplier)
          ref ! DemandWorker.HandleSupplier(supplier)
        case None           ⇒
          ref ! DemandWorker.NoMoreSuppliers
      }
    
    case event:SupplyUserLoggedIn ⇒
      addSupplier(event.supplier)

    case event:SupplyUserLoggedOut ⇒
      removeSupplier(event.supplier)

    case any ⇒
      log.logUnhandledMessage(any)
  }

  def worker(demand:DemandRepr):ActorRef
}

/**
 * An implementation that spawns `DemandWorker` actors for handling a service request.
 * @param groupId   The identifier of the service this actor manages demand for.
 */
class DemandManagerImpl(override val groupId:DemandGroupID) extends DemandManager with MockedSupplierResolving
{
  /**
   * Spawns a worker if the worker does not exist already.
   * @param demand    The demand to create a worker for
   * @return          The worker that is spawned, or already existed.
   */
  def worker(demand:DemandRepr):ActorRef = {
    context.child(DemandWorker.name(demand.id)) match {
      case Some(ref)  ⇒ ref
      case None       ⇒ context.actorOf(DemandWorker.props(demand), DemandWorker.name(demand.id))
    }
  }
}
