package nl.bluesoft.ondemand.demand.system
import java.time.LocalDateTime
import akka.actor.{ActorLogging, Props}
import nl.bluesoft.ondemand.common.ops.LoggingAdapterOps
import nl.bluesoft.ondemand.common.representation.RepresentationTypes._
import nl.bluesoft.ondemand.common.representation._
import nl.bluesoft.ondemand.demand.system.DemandRequest.State

private[demand] object DemandRequest {
  def props(id:DemandID) = Props(new DemandRequestImpl(id))
  def name(id:DemandID) = s"DemandRequest-$id"

  case class Create(demand:DemandRepr)
  case object GetState

  case class SupplierAttempted(supplier:SupplyUserRepr)
  case class SupplierAcknowledged(supplier:SupplyUserRepr)

  case class State(id:DemandID, groupID: DemandGroupID, title:String, description:String, timestamp:LocalDateTime,
                   demander:DemandUserRepr, attemptedSuppliers:Seq[Username] = Seq.empty, servicingSupplier:Option[SupplyUserRepr] = None)
}

trait DemandRequest extends RepresentableActor[DemandRequest.State, DemandRepr] with ActorLogging with LoggingAdapterOps
{
  def id:DemandID
  var state:DemandRequest.State = _

  override def representationOf(state: State): DemandRepr = DemandRepr(
    id = state.id,
    groupID = state.groupID,
    title = state.title,
    description = state.description,
    timestamp = state.timestamp,
    demander = state.demander,
    attemptedSuppliers = state.attemptedSuppliers,
    servicingSupplier = state.servicingSupplier
  )

  override def receive: Receive = uninitialized

  def uninitialized:Receive = {
    case event @ DemandRequest.Create(demand) ⇒
      state = DemandRequest.State(
        id = demand.id,
        groupID = demand.groupID,
        title = demand.title,
        description = demand.description,
        timestamp = demand.timestamp,
        demander = demand.demander
      )
      context.become(created)
      log.info("Created demand request.")

    case unhandled ⇒
      log.logUnhandledMessage(unhandled, "The state is uninitialized.")
  }

  def created:Receive = {
    case event @ DemandRequest.Create(demand) ⇒
      log.info("DemandRequest.Create was received. But the demand is already created.")

    case event:DemandRequest.SupplierAttempted ⇒
      log.info(s"Storing attempted supplier ${event.supplier.username}")
      state = state.copy(attemptedSuppliers = state.attemptedSuppliers :+ event.supplier.username)

    case event:DemandRequest.SupplierAcknowledged ⇒
      log.info(s"Storing the servicing supplier ${event.supplier.username}.")
      state = state.copy(servicingSupplier = Some(event.supplier))

    case cmd @ DemandRequest.GetState ⇒
      sender ! representationOf(state)

    case unhandled ⇒
      log.logUnhandledMessage(unhandled, "The state is created.")
  }
}

class DemandRequestImpl(override val id:DemandID) extends DemandRequest
{

}