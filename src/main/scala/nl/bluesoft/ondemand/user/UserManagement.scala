package nl.bluesoft.ondemand.user
import java.time.LocalDateTime
import nl.bluesoft.ondemand.common.representation.RepresentationTypes
import RepresentationTypes._
import nl.bluesoft.ondemand.common.annotation.mocked
import nl.bluesoft.ondemand.user.system.User

import scala.concurrent.Future

trait UserManagement extends PasswordHashing
{
  this:User ⇒
  import RepresentationTypes._

  def createUser(username:Username, password:Password): Future[User.State]
}

@mocked(message = "The user management is mocked and does not persist anything.")
trait MockedUserManagement {
  this:UserManagement with User ⇒

  override def createUser(username:Username, password:Password): Future[User.State] = {
    val hashAndSalt = hashPassword(password)
    val state = User.State(username, hashAndSalt._1, hashAndSalt._2, LocalDateTime.now())
    Future.successful(state)
  }
}