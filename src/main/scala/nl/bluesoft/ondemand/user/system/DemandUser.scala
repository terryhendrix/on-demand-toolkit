package nl.bluesoft.ondemand.user.system
import nl.bluesoft.ondemand.common.representation.{DemandRepr, DemandUserRepr, RepresentationTypes}
import RepresentationTypes.{UserType, UserTypes}
import nl.bluesoft.ondemand.common.event.{DemandRequestFailed, DemandRequestCompleted}
import nl.bluesoft.ondemand.user._

private[user] object DemandUser {
  case class RequestDemand(demand:DemandRepr)
}

private[user] trait DemandUser extends User with MockedPasswordHashing with DemandUserEventPublishing
{
  override def representationOf(state:User.State):DemandUserRepr = DemandUserRepr(state.username)

  override def userType: UserType = UserTypes.demander

  override def servicing: Receive = {
    case cmd:DemandUser.RequestDemand ⇒
      log.info("Requesting demand")
      publishDemandRequested(cmd.demand)

    case event:DemandRequestCompleted ⇒
      log.info("Demand request completed successful.")
      pushNotification(event)

    case event:DemandRequestFailed ⇒
      log.info(s"Demand request failed: ${event.reason}")
      pushNotification(event)
  }
}

private[user] class DemandUserImpl extends DemandUser with DemandUserEventStreamPublishing with MockedUserManagement
                     with EventStreamPushNotifications
