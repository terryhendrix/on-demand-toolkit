package nl.bluesoft.ondemand.user.system
import nl.bluesoft.ondemand.common.event.{ServiceRequestRetracted, ServiceRequested}
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.{DemandID, UserType, UserTypes, Username}
import nl.bluesoft.ondemand.common.representation.{DemandUserRepr, SupplyUserRepr}
import nl.bluesoft.ondemand.user._

private[user] object SupplyUser {
  case class RespondToServiceRequest(demand:DemandID, supplier:Username, acknowledged:Boolean)
}

private[user] trait SupplyUser extends User with MockedPasswordHashing with SupplyUserEventPublishing
{
  override def representationOf(state:User.State):SupplyUserRepr = SupplyUserRepr(state.username)

  override def userType: UserType = UserTypes.supplier

  override def servicing: Receive = {
    case cmd:SupplyUser.RespondToServiceRequest ⇒
      publishServiceResponse(cmd.demand, representationOf(state), cmd.acknowledged)
    case event:ServiceRequested ⇒
      pushNotification(event)
    case event:ServiceRequestRetracted ⇒
      pushNotification(event)
  }
}

private[user] class SupplyUserImpl extends SupplyUser with SupplyUserEventStreamPublishing with MockedUserManagement
                     with EventStreamPushNotifications
