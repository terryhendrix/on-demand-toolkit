package nl.bluesoft.ondemand.user.system
import java.time.LocalDateTime

import akka.actor.{ActorLogging, Props}
import nl.bluesoft.ondemand.common.event.FalseLoginAttempted
import nl.bluesoft.ondemand.common.representation.{RepresentableActor, RepresentationTypes, UserRepr}
import nl.bluesoft.ondemand.user._
import nl.bluesoft.ondemand.user.system.User.GetState

private[user] object User
{
  import RepresentationTypes._

  def props(userType:UserType) = userType match {
    case UserTypes.demander ⇒ demanderProps
    case UserTypes.supplier ⇒ supplierProps
  }

  def demanderProps = Props(new DemandUserImpl)

  def supplierProps = Props(new SupplyUserImpl)

  case class State(username:Username, password:HashedPassword, salt:Salt, created:LocalDateTime)

  case class CreateUser(username:String, password:Password)
  case class UserAlreadyExists(username:String)

  case class Authenticate(username:String, password:String, userType: UserType)
  case class Authenticated(username:String)

  case object GetState

  sealed case class UserCreated(state:User.State)
}

/**
 * An actor abstraction over an end-user.
 * A `User` starts in the `uninitialized` state, Responding only to `CreateUser` and the internal message `UserCreated`.
 * After receiving the `CreateUser` message, and the user is created, the actor moves to the `created` stage.
 */
private[user] trait User extends RepresentableActor[User.State, UserRepr] with ActorLogging with UserManagement with EventPublishing with PasswordHashing with PushNotifications
{
  import RepresentationTypes._

  implicit lazy val ec = context.dispatcher

  var state:User.State = _

  def userType:UserType

  override def receive: Receive = uninitialized

  def uninitialized:Receive = {
    case User.CreateUser(username, password) ⇒
      log.info(s"Creating $userType.")
      val ref = sender()
      createUser(username, password).map { state ⇒
        self ! User.UserCreated(state)
        ref ! UserManager.UserCreated(representationOf(state))
        publishCreated(state)
      }
    case event:User.UserCreated ⇒
      log.info("User created.")
      this.state = event.state
      context.become(created)
    case cmd:User.Authenticate ⇒
      log.info("Login attempt for non existing user.")
      sender ! FalseLoginAttempted(cmd.username)
      context.system.eventStream.publish(FalseLoginAttempted(cmd.username))
  }

  def created:Receive = {
    case User.CreateUser(username, password) ⇒
      sender ! User.UserAlreadyExists(username)

    case User.Authenticate(username, password, userType) if hashPassword(password, state.salt) == state.password && userType == this.userType ⇒
      log.info("Authentication successful.")
      context.parent ! UserManager.UserAuthentication(representationOf(state))
      sender ! User.Authenticated(username)

    case User.Authenticate(username, _, _) ⇒
      log.warning("Authentication failed.")
      sender ! FalseLoginAttempted(username)
      context.system.eventStream.publish(FalseLoginAttempted(username))

    case GetState ⇒
      sender ! representationOf(state)

    case any if servicing.isDefinedAt(any) ⇒
      servicing(any)
  }

  /**
   * The service `Receive` block allows you to add functionality during the `created` stage.
   * @return
   */
  def servicing:Receive
}