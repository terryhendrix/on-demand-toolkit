package nl.bluesoft.ondemand.user.system

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import nl.bluesoft.ondemand.common.event._
import nl.bluesoft.ondemand.common.ops.{ConfigOps, LoggingAdapterOps}
import nl.bluesoft.ondemand.common.representation.RepresentationTypes._
import nl.bluesoft.ondemand.common.representation.{DemandRepr, DemandUserRepr, SupplyUserRepr, UserRepr}
import nl.bluesoft.ondemand.user.UserModule

object UserManager {
  def props = Props(new UserManagerImpl())
  case class Register(username:Username, password:Password, userType: UserType)
  case class Login(username:Username, password:Password, userType:UserType)
  case class UserCreated(user:UserRepr)
  case class GetUser(username:Username, userType: UserType)
  case class RespondToServiceRequest(demand:DemandID, supplier:Username, acknowledged:Boolean)
  case class RequestDemand(demand:DemandRepr)
  case class UserAuthentication(user:UserRepr)
  case class SessionInvalidated(username:Username)
}

private[user] trait UserManager extends Actor with ActorLogging with LoggingAdapterOps with ConfigOps
{
  implicit val timeout = context.system.settings.config.getTimeout(UserModule.ConfigKeys.FacadeTimeout)
  implicit val ec = context.system.dispatcher

  override def preStart() = {
    context.system.eventStream.subscribe(self, classOf[ServiceRequested])
    context.system.eventStream.subscribe(self, classOf[ServiceRequestRetracted])
    context.system.eventStream.subscribe(self, classOf[DemandRequestCompleted])
    context.system.eventStream.subscribe(self, classOf[DemandRequestFailed])
  }

  override def postStop() = {
    context.system.eventStream.unsubscribe(self)
  }

  override def receive:Receive = {
    case cmd:UserManager.RespondToServiceRequest ⇒
      val demander = getUser(cmd.supplier, UserTypes.demander)
      log.info(s"Routing RespondToServiceRequest message to ${cmd.supplier}.")
      demander ! SupplyUser.RespondToServiceRequest(cmd.demand, cmd.supplier, cmd.acknowledged)

    case event:UserManager.UserAuthentication ⇒
      event.user match {
        case user:SupplyUserRepr  ⇒ context.system.eventStream.publish(SupplyUserLoggedIn(user))
        case user:DemandUserRepr  ⇒ log.info(s"Demand user ${user.username} logged in.")
      }

    case event:UserManager.SessionInvalidated ⇒
      log.info(s"Session of user ${event.username} has invalidated.")
      ???   // TODO: Implement, send supply user logged out

    case cmd:UserManager.RequestDemand ⇒
      log.info(s"Routing RequestDemand message to ${cmd.demand.demander.username}.")
      getUser(cmd.demand.demander.username, UserTypes.demander) ! DemandUser.RequestDemand(cmd.demand)

    case cmd:UserManager.GetUser ⇒
      sender ! getUser(cmd.username, cmd.userType)

    case event:ServiceRequested ⇒
      log.info(s"Routing ServiceRequested message to ${event.supplier.username}.")
      getUser(event.supplier.username, UserTypes.supplier) ! event

    case event:ServiceRequestRetracted ⇒
      log.info(s"Routing ServiceRequestRetracted message to ${event.supplier.username}.")
      getUser(event.supplier.username, UserTypes.supplier) ! event

    case event:DemandRequestCompleted ⇒
      log.info(s"Routing DemandRequestCompleted message to ${event.demand.demander.username}.")
      getUser(event.demand.demander.username, UserTypes.demander) ! event

    case event:DemandRequestFailed ⇒
      log.info(s"Routing DemandRequestFailed message to ${event.demand.demander.username}.")
      getUser(event.demand.demander.username, UserTypes.demander) ! event

    case cmd:UserManager.Register ⇒
      log.info("Registering new user")
      getUser(cmd.username, cmd.userType) forward User.CreateUser(cmd.username, cmd.password)

    case cmd:UserManager.Login ⇒
      log.info(s"Routing login request to ${cmd.username}")
      getUser(cmd.username, cmd.userType) forward User.Authenticate(cmd.username, cmd.password, cmd.userType)

    case unhandledMessage ⇒
      log.logUnhandledMessage(unhandledMessage)
  }

  def getUser(username: String, userType: UserType): ActorRef = {
    context.child(username) match {
      case Some(ref) ⇒ ref
      case None ⇒ context.actorOf(User.props(userType), username)
    }
  }
}

private[user] class UserManagerImpl extends UserManager
