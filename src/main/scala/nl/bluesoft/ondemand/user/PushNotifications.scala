package nl.bluesoft.ondemand.user
import nl.bluesoft.ondemand.common.event.{PushNotificationReceived, PushNotification}
import nl.bluesoft.ondemand.user.system.User

trait PushNotifications
{ this:User ⇒

  def pushNotification[U](notification:U):Unit

  final def createPushNotification[U](notification:U):PushNotificationReceived[U] = {
    PushNotification(representationOf(state), notification)
  }
}

trait EventStreamPushNotifications {
  this:PushNotifications with User ⇒

  def pushNotification[U](notification:U) = {
    val pushMessage = createPushNotification(notification)
    context.system.eventStream.publish(pushMessage)
  }
}