package nl.bluesoft.ondemand.user
import nl.bluesoft.ondemand.common.representation.RepresentationTypes
import RepresentationTypes._
import nl.bluesoft.ondemand.common.annotation.mocked

/**
 * The `PasswordHashing` trait defines abstract methods for hashing a password using salts.
 */
trait PasswordHashing
{
  /**
   * Hashes a `Password` using a new salt.
   * @param password    The `Password` to hash.
   * @return            A tuple of 2 fields. In field _1 the `HashedPassword`, in field _2 the `Salt`.
   */
  def hashPassword(password:Password):(HashedPassword, Salt)

  /**
   * Hashes a password using a already determined salt.
   * @param password    The `Password` to hash.
   * @param salt        The `Salt` to use while hashing.
   * @return            The hashed password
   */
  def hashPassword(password:Password, salt:Salt):HashedPassword
}

/**
 * Mocking `PasswordHashing`, does nothing with the `Salt` and does not hash.
 * It just same the plain `Password` that you put in.
 */
@mocked(message = "The password functionality is not yet implemented. It returns plain text passwords and a default salt.")
trait MockedPasswordHashing {
  this:PasswordHashing ⇒

  def hashPassword(password:Password):(HashedPassword, Salt) = {
    (password, "abcdef")
  }

  def hashPassword(password:Password, salt:Salt):HashedPassword = {
    password
  }
}
