package nl.bluesoft.ondemand.user
import nl.bluesoft.ondemand.common.event.{DemandRequested, DemandUserCreated, ServiceResponseReceived, SupplyUserCreated}
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.DemandID
import nl.bluesoft.ondemand.common.representation.{DemandRepr, DemandUserRepr, SupplyUserRepr}
import nl.bluesoft.ondemand.user.system.{DemandUser, SupplyUser, User}

trait EventPublishing {
  this:User ⇒

  def publishCreated(state:User.State):Unit
}

trait DemandUserEventPublishing {
  this:DemandUser ⇒
  def publishDemandRequested(demand:DemandRepr):Unit
}

trait SupplyUserEventPublishing {
  this:SupplyUser ⇒
  def publishServiceResponse(demand:DemandID, supplier:SupplyUserRepr, acknowledged:Boolean):Unit
}


trait DemandUserEventStreamPublishing extends DemandUserEventPublishing {
  this:DemandUser ⇒

  def publishDemandRequested(demand:DemandRepr):Unit = {
    context.system.eventStream.publish(DemandRequested(demand))
  }

  def publishCreated(state:User.State):Unit = {
    context.system.eventStream.publish(DemandUserCreated(DemandUserRepr(state.username)))
  }
}

trait SupplyUserEventStreamPublishing extends SupplyUserEventPublishing {
  this:SupplyUser ⇒

  def publishServiceResponse(demand:DemandID, supplier:SupplyUserRepr, acknowledged:Boolean):Unit = {
    context.system.eventStream.publish(ServiceResponseReceived(demand, supplier, acknowledged))
  }

  def publishCreated(state:User.State):Unit = {
    context.system.eventStream.publish(SupplyUserCreated(SupplyUserRepr(state.username)))
  }
}