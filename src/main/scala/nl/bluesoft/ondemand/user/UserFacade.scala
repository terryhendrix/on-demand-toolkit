package nl.bluesoft.ondemand.user
import akka.actor.{ActorRef, ActorSystem}
import akka.event.Logging
import akka.pattern._
import nl.bluesoft.ondemand.api.system.session.SessionManager
import nl.bluesoft.ondemand.common.ops.{LoggingAdapterOps, ConfigOps}
import nl.bluesoft.ondemand.common.representation.RepresentationTypes
import RepresentationTypes.{Password, UserType, Username}
import nl.bluesoft.ondemand.user.system.{User, UserManager}

import scala.concurrent.Future

trait UserFacade
{
  def userManager:ActorRef

  def registerUser(username:Username, password:Password, userType: UserType):Future[Boolean]

  def loginUser(username:Username, password:Password, userType: UserType):Future[Boolean]
}

object UserModule {
  def apply(system:ActorSystem): UserFacade =  {
    val module = new UserModule(system)
    module.preStart()
    module
  }

  object ConfigKeys {
    val FacadeTimeout = "user.facade-timeout"
    val GenericTimeout = "user.timeout"
  }
}

/**
 * The `UserModule` implements a facade, it hides internal complexity.
 * @param system  The `ActorSystem` to create the actors of the module in.
 */
private[user] class UserModule(system:ActorSystem) extends UserFacade with LoggingAdapterOps with ConfigOps
{
  implicit val timeout = system.settings.config.getTimeout(UserModule.ConfigKeys.FacadeTimeout)
  implicit val ec = system.dispatcher
  val log = Logging(system, "UserModule")

  override val userManager = system.actorOf(UserManager.props, "UserManager")

  def preStart() = {
    log.info("Started UserModule")
  }

  override def registerUser(username:Username, password:Password, userType: UserType) = {
    userManager.ask(UserManager.Register(username, password, userType)).map {
      case event:UserManager.UserCreated ⇒ true
      case _                             ⇒ false
    }.recover { case ex ⇒
      log.logException(s"create user $username", ex)
      false
    }
  }

  override def loginUser(username:Username, password:Password, userType: UserType) = {
    userManager.ask(UserManager.Login(username, password, userType)).map {
      case event:User.Authenticated ⇒ true
      case _                        ⇒ false
    }.recover { case ex ⇒
      log.logException(s"login user $username", ex)
      false
    }
  }
}
