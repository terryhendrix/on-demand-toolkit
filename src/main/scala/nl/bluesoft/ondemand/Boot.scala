package nl.bluesoft.ondemand
import akka.actor.ActorSystem
import nl.bluesoft.ondemand.api.ApiModule

object Boot extends App
{
  val system = ActorSystem("Demander")
  val api = ApiModule(system)
}
