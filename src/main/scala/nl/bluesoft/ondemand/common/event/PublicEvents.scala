package nl.bluesoft.ondemand.common.event
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.DemandID
import nl.bluesoft.ondemand.common.representation._

/**
 * A new demand is requested by a demand user.
 * @param demand        The demand the user has.
 */
case class DemandRequested(demand:DemandRepr)

/**
 * A demand fulfilment is requested from a supplier.
 * @param demand        The demand to be fulfilled.
 * @param supplier      The supplier who may fulfill the demand.
 */
case class ServiceRequested(demand:DemandRepr, supplier:SupplyUserRepr)

/**
 * A service request is retracted. The supply user can no longer reply.
 * @param demand        The demand to be fulfilled.
 * @param supplier      The supplier who may fulfill the demand.
 */
case class ServiceRequestRetracted(demand:DemandRepr, supplier:SupplyUserRepr)

/**
 * A demand request has completed.
 * @param demand        The demand to be fulfilled.
 * @param supplier      The supplier who may fulfill the demand.
 */
case class DemandRequestCompleted(demand:DemandRepr, supplier:SupplyUserRepr)

/**
 * A demand request has failed.
 * @param demand        The demand to be fulfilled.
 * @param reason        The reason why the request failed.
 */
case class DemandRequestFailed(demand:DemandRepr, reason:String)

/**
 * Indicates that a supplier has responded to a `ServiceRequested` event.
 * @param demand        The demand the reply is about.
 * @param supplier      The supplier that was asked to fulfill the demand.
 * @param acknowledged  Does the supplier acknowledge the request?
 *                      `true` if the supplier will fulfill the request.
 *                      `false` if the supplier will not fulfill the request.
 */
case class ServiceResponseReceived(demand:DemandID, supplier:SupplyUserRepr, acknowledged:Boolean)

/**
 * Indicates that a supply user has logged in.
 * @param supplier      The supplier that logged in.
 */
case class SupplyUserLoggedIn(supplier:SupplyUserRepr)

/**
 * Indicates that a supply user has logged out.
 * @param supplier      The supplier that logged out.
 */
case class SupplyUserLoggedOut(supplier:SupplyUserRepr)

/**
 * Indicates that a supply user has been created.
 * @param user  The representation of the just created user.
 */
case class SupplyUserCreated(user:SupplyUserRepr)

/**
 * Indicates that a demand user has been created
 * @param user  The representation of the just created user.
 */
case class DemandUserCreated(user:DemandUserRepr)

/**
 * Indicates that there is a false login attempt by the username.
 * @param username  The username that was used in the false login attempt.
 */
case class FalseLoginAttempted(username:String)


object PushNotification {
  def apply[U](user:UserRepr, notification:U):PushNotificationReceived[U] = {
    PushNotificationReceived(user, notification.getClass.getSimpleName, notification)
  }
}

/**
 * 
 * @param user
 * @param className
 * @param notification
 * @tparam U
 */
case class PushNotificationReceived[U](user:UserRepr, className:String, notification:U)