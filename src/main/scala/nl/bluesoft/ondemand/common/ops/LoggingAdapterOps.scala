package nl.bluesoft.ondemand.common.ops

import akka.event.LoggingAdapter

trait LoggingAdapterOps
{
  implicit class LoggingAdapterOps(log:LoggingAdapter) {
    def logUnhandledMessage(message:Any, logMessage:String = "") = {
      log.warning(s"Unhandled message $message. $logMessage")
    }

    def logException(action:String, t:Throwable):Unit = {
      log.error(s"Exception occurred while performing '{}'. {}: {}", action, t.getClass.getName, t.getMessage)
      logCause(t)

      if(log.isDebugEnabled)
        t.printStackTrace()
    }

    def logCause(t:Throwable):Unit = {
      Option(t.getCause).foreach { cause ⇒
        log.error(s"Caused by {}: {}", t.getClass.getName, t.getMessage)
        logCause(cause)
      }
    }

    def logSecurityViolation(message:String) = {
      log.warning("Security violation: {}", message)
    }
  }
}
