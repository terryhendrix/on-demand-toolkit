package nl.bluesoft.ondemand.common.ops

import java.util.concurrent.TimeUnit

import akka.util.Timeout
import com.typesafe.config.Config

import scala.concurrent.duration.{Duration, FiniteDuration}

trait ConfigOps
{
  implicit class ConfigOps(config:Config) {
    def getFiniteDuration(key:String) = {
      FiniteDuration(Duration(config.getString(key)).toMillis, TimeUnit.MILLISECONDS)
    }

    def getTimeout(key:String) = {
      Timeout(FiniteDuration(Duration(config.getString(key)).toMillis, TimeUnit.MILLISECONDS))
    }
  }
}
