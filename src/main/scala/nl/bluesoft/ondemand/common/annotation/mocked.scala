package nl.bluesoft.ondemand.common.annotation

/**
 * Indicates that a trait, class or object contains a mocked implementation.
 * @param message   The message that explains what the mock does.
 */
class mocked(val message:String = "") extends deprecated(message, "start")
