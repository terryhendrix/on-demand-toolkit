package nl.bluesoft.ondemand.common.representation

import java.time.LocalDateTime

import akka.actor.Actor
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.{SessionID, DemandGroupID, DemandID, Username}

object RepresentationTypes {
  type Username = String
  type Password = String
  type HashedPassword = String
  type Salt = String
  type DemandGroupID = String
  type SessionID = String
  type DemandID = String

  type SourceIpAddress = String

  type UserType = String
  object UserTypes {
    val demander:UserType = "demander"
    val supplier:UserType = "supplier"
  }
}

trait Representation[State, Repr] {
  def representationOf(state:State):Repr
}

trait RepresentableActor[State, Repr] extends Actor with Representation[State, Repr]

trait UserRepr {
  def username:Username
}

case class SupplyUserRepr(username:Username) extends UserRepr

case class DemandUserRepr(username:Username) extends UserRepr

case class DemandRepr(id:DemandID, groupID: DemandGroupID, title:String, description:String, timestamp:LocalDateTime,
                      demander:DemandUserRepr, attemptedSuppliers:Seq[Username] = Seq.empty,
                      servicingSupplier:Option[SupplyUserRepr] = None)

case class SessionRepr(username:Username, id:SessionID)