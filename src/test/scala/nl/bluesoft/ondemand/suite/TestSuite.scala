package nl.bluesoft.ondemand.suite

import java.time.LocalDateTime
import java.util.UUID

import akka.actor.ActorSystem
import akka.testkit.TestProbe
import nl.bluesoft.ondemand.common.event._
import nl.bluesoft.ondemand.common.representation.RepresentationTypes._
import nl.bluesoft.ondemand.common.representation.{DemandRepr, DemandUserRepr, SupplyUserRepr}
import nl.bluesoft.ondemand.suite.TestSuite.EventStreamTestProbe
import org.scalatest.concurrent.{Eventually, ScalaFutures}
import org.scalatest.time.{Millis, Second, Span}
import org.scalatest.{BeforeAndAfterEach, FeatureSpecLike, GivenWhenThen, Matchers}

import scala.concurrent.duration._

object TestSuite {
  var globalSystem:ActorSystem = _

  case class EventStreamTestProbe() extends TestProbe(globalSystem) {
    def subscribe() = {
      globalSystem.eventStream.subscribe(ref, classOf[SupplyUserCreated])
      globalSystem.eventStream.subscribe(ref, classOf[SupplyUserLoggedIn])
      globalSystem.eventStream.subscribe(ref, classOf[SupplyUserLoggedOut])
      globalSystem.eventStream.subscribe(ref, classOf[PushNotificationReceived[_]])
      globalSystem.eventStream.subscribe(ref, classOf[DemandUserCreated])
      globalSystem.eventStream.subscribe(ref, classOf[ServiceResponseReceived])
      globalSystem.eventStream.subscribe(ref, classOf[DemandRequested])
      globalSystem.eventStream.subscribe(ref, classOf[ServiceRequested])
      globalSystem.eventStream.subscribe(ref, classOf[ServiceRequestRetracted])
      globalSystem.eventStream.subscribe(ref, classOf[DemandRequestFailed])
      globalSystem.eventStream.subscribe(ref, classOf[ServiceRequested])
      globalSystem.eventStream.subscribe(ref, classOf[DemandRequestCompleted])
      globalSystem.eventStream.subscribe(ref, classOf[FalseLoginAttempted])
    }

    def unsubscribe() = globalSystem.eventStream.unsubscribe(ref)

    def publish(message:AnyRef) = {
      system.eventStream.publish(message)
      expectMsg(message)  // Expect the message cause we just placed it on the stream.
    }
  }

}

trait TestSuite extends FeatureSpecLike with BeforeAndAfterEach with Eventually with GivenWhenThen with Matchers with ScalaFutures
{
  implicit var _system:ActorSystem = _
  implicit def log = _system.log
  implicit override val patienceConfig = PatienceConfig(Span(1, Second), Span(100, Millis))

  var eventStream:EventStreamTestProbe = _

  val sizeOfUUID = UUID.randomUUID().toString.length

  def expectNoMessageDuration = 30 millis

  def createDemand(demander:DemandUserRepr, demandGroupID: DemandGroupID = UUID.randomUUID().toString) = {
    DemandRepr(
      id = UUID.randomUUID().toString,
      groupID = demandGroupID,
      title = "Title",
      description = "Description",
      timestamp = LocalDateTime.now(),
      demander = demander
    )
  }

  val demander1 = DemandUserRepr("terry")
  val demander2 = DemandUserRepr("babs")
  val demander3 = DemandUserRepr("polly")

  val supplier1 = SupplyUserRepr("henk")
  val supplier2 = SupplyUserRepr("willem")
  val supplier3 = SupplyUserRepr("pietje")

  val sourceIpAddress1:SourceIpAddress = "8.8.8.8"
  val sourceIpAddress2:SourceIpAddress = "127.0.0.1"
  val sourceIpAddress3:SourceIpAddress = "4.4.4.4"

  override def beforeEach() = {
    _system = ActorSystem("OnDemand")
    TestSuite.globalSystem = _system
    eventStream = EventStreamTestProbe()
    eventStream.subscribe()
    Thread sleep 1000
  }

  override def afterEach() = {
    eventStream.unsubscribe()
    _system.shutdown()
    _system.awaitTermination()
    TestSuite.globalSystem = null
    _system = null
  }
}
