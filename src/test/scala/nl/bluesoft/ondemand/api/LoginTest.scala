package nl.bluesoft.ondemand.api

import akka.testkit.TestProbe
import nl.bluesoft.ondemand.api.system.session.SessionManager
import nl.bluesoft.ondemand.common.representation.RepresentationTypes
import RepresentationTypes.UserTypes
import nl.bluesoft.ondemand.common.event.{DemandUserCreated, SupplyUserCreated}
import nl.bluesoft.ondemand.suite.TestSuite

//TODO: Add calls to the session manager to see that a session is indeed created and maintained.
class LoginTest extends TestSuite
 {
   feature("As a user, i must be able to login with my credentials") {
     scenario("1. As a demand user, i must be able to login") {
       val api = ApiModule(_system).asInstanceOf[ApiModule]
       val probe = TestProbe()

       withClue("Creating a user must be successful") {
         api.createUser(sourceIpAddress1, demander1.username, "1234", UserTypes.demander).futureValue
         eventStream.expectMsg(DemandUserCreated(demander1))
         eventStream.expectNoMsg(expectNoMessageDuration)
       }

       withClue("Logging in must successful if the correct credentials are used") {
         val session = api.loginUser(sourceIpAddress1, demander1.username, "1234", UserTypes.demander).futureValue
         session.username shouldBe demander1.username
         session.id.size shouldBe sizeOfUUID

         probe.send(api.sessionManager, SessionManager.ValidateSession(session.id))
         probe.expectMsg(SessionManager.ValidatedSession(session))
       }

       withClue("With false credentials or userType login fails") {
         intercept[RuntimeException](api.loginUser(sourceIpAddress1, demander1.username, "12345", UserTypes.demander).futureValue)
         intercept[RuntimeException](api.loginUser(sourceIpAddress1, demander1.username, "1234", UserTypes.supplier).futureValue)
         intercept[RuntimeException](api.loginUser(sourceIpAddress1, "nonexisting", "1234", UserTypes.demander).futureValue)
       }
     }

     scenario("2. As a supply user, i must be able to login") {
       val api = ApiModule(_system).asInstanceOf[ApiModule]
       val probe = TestProbe()

       withClue("Creating a user must be successful") {
         api.createUser(sourceIpAddress1, supplier1.username, "1234", UserTypes.supplier).futureValue
         eventStream.expectMsg(SupplyUserCreated(supplier1))
         eventStream.expectNoMsg(expectNoMessageDuration)
       }

       withClue("Logging in must successful if the correct credentials are used") {
         val session = api.loginUser(sourceIpAddress1, supplier1.username, "1234", UserTypes.supplier).futureValue
         session.username shouldBe supplier1.username
         session.id.size shouldBe sizeOfUUID

         probe.send(api.sessionManager, SessionManager.ValidateSession(session.id))
         probe.expectMsg(SessionManager.ValidatedSession(session))
       }

       withClue("With false credentials or userType login fails") {
         intercept[RuntimeException](api.loginUser(sourceIpAddress1, supplier1.username, "12345", UserTypes.supplier).futureValue)
         intercept[RuntimeException](api.loginUser(sourceIpAddress1, supplier1.username, "1234", UserTypes.demander).futureValue)
         intercept[RuntimeException](api.loginUser(sourceIpAddress1, "nonexisting", "1234", UserTypes.supplier).futureValue)
       }
     }
   }
 }
