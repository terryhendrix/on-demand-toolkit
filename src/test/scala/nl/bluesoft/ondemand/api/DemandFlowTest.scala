package nl.bluesoft.ondemand.api

import akka.testkit.TestProbe
import nl.bluesoft.ondemand.common.event._
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.UserTypes
import nl.bluesoft.ondemand.common.representation.SessionRepr
import nl.bluesoft.ondemand.suite.TestSuite

// TODO: Add test for service denials and service timeouts
class DemandFlowTest extends TestSuite
{
  var supplier1Session: SessionRepr = _
  var demander1Session: SessionRepr = _

  feature("As end users, we want to communicate with each other.") {
    scenario("1a. As a demand user, my demand must be acknowledged by a supplier. " +
      "1b. As a supply user, i must be able to acknowledge a demand.") {
      val api = ApiModule(_system).asInstanceOf[ApiModule]
      val probe = TestProbe()
      val demand1 = createDemand(demander1)

      withClue("As a demand user, i must be able to create an account and login.") {
        api.createUser(sourceIpAddress1, demander1.username, "1234", UserTypes.demander).futureValue
        demander1Session = api.loginUser(sourceIpAddress1, demander1.username, "1234", UserTypes.demander).futureValue

        eventStream.expectMsg(DemandUserCreated(demander1))
        eventStream.expectNoMsg(expectNoMessageDuration)
      }

      withClue("As a supply user, i must be able to create an account and login.") {
        api.createUser(sourceIpAddress1, supplier1.username, "1234", UserTypes.supplier).futureValue
        eventStream.expectMsg(SupplyUserCreated(supplier1))
        eventStream.expectNoMsg(expectNoMessageDuration)

        supplier1Session = api.loginUser(sourceIpAddress1, supplier1.username, "1234", UserTypes.supplier).futureValue
        eventStream.expectMsg(SupplyUserLoggedIn(supplier1))
        eventStream.expectNoMsg(expectNoMessageDuration)
      }

      withClue("As a demand user, my demand must be acknowledged by a supplier.") {
        api.requestDemand(sourceIpAddress1, demand1)(demander1Session.id)
        eventStream.expectMsgAllOf(
          DemandRequested(demand1),
          ServiceRequested(demand1, supplier1),
          PushNotificationReceived(supplier1, "ServiceRequested", ServiceRequested(demand1, supplier1))
        )
        eventStream.expectNoMsg(expectNoMessageDuration)
      }

      withClue("As a supply user, i must be able to acknowledge a demand.") {
        api.respondToDemand(sourceIpAddress2, demand1.id, acknowledged = true)(supplier1Session.id)
        eventStream.expectMsgAllOf(
          ServiceResponseReceived(demand1.id, supplier1, acknowledged = true),
          DemandRequestCompleted(demand1, supplier1),
          PushNotificationReceived(demander1, "DemandRequestCompleted", DemandRequestCompleted(demand1, supplier1))
        )
        eventStream.expectNoMsg(expectNoMessageDuration)
      }
    }
  }
}
