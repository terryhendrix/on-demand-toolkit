package nl.bluesoft.ondemand.api
import nl.bluesoft.ondemand.common.representation.RepresentationTypes
import RepresentationTypes.UserTypes
import nl.bluesoft.ondemand.common.event.{DemandUserCreated, SupplyUserCreated}
import nl.bluesoft.ondemand.suite.TestSuite

class CreateUserTest extends TestSuite
{
  feature("As a user, i must be able to create an account") {
    scenario("1. As a demand user, i must be ale to create an account") {
      val api = ApiModule(_system)

      withClue("Creating a user must be successful") {
        api.createUser(sourceIpAddress1, demander1.username, "1234", UserTypes.demander).futureValue
        eventStream.expectMsg(DemandUserCreated(demander1))
        eventStream.expectNoMsg(expectNoMessageDuration)
      }

      withClue("Creating the same user again must fail") {
        intercept[RuntimeException](api.createUser(sourceIpAddress2, demander1.username, "1234", UserTypes.demander).futureValue)
        intercept[RuntimeException](api.createUser(sourceIpAddress2, demander1.username, "1234", UserTypes.supplier).futureValue)
        eventStream.expectNoMsg(expectNoMessageDuration)
      }
    }

    scenario("2. As a supply user, i must be able to create an account") {
      val api = ApiModule(_system)

      withClue("Creating a user must be successful") {
        api.createUser(sourceIpAddress1, supplier1.username, "1234", UserTypes.supplier).futureValue
        eventStream.expectMsg(SupplyUserCreated(supplier1))
        eventStream.expectNoMsg(expectNoMessageDuration)
      }

      withClue("Creating the same user again must fail") {
        intercept[RuntimeException](api.createUser(sourceIpAddress2, supplier1.username, "1234", UserTypes.supplier).futureValue)
        intercept[RuntimeException](api.createUser(sourceIpAddress2, supplier1.username, "1234", UserTypes.demander).futureValue)
        eventStream.expectNoMsg(expectNoMessageDuration)
      }
    }
  }
}
