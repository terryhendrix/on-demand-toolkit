package nl.bluesoft.ondemand.api
import akka.actor.ActorSystem
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.testkit.ScalatestRouteTest
import nl.bluesoft.ondemand.api.rest.RestApi
import nl.bluesoft.ondemand.api.rest.RestApi.RestApiJsonProtocol
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.UserTypes
import nl.bluesoft.ondemand.common.representation.SessionRepr
import nl.bluesoft.peanuts.json.JsonMarshalling
import org.scalatest.concurrent.ScalaFutures
import org.scalatest._


class RestApiTest extends FeatureSpec with Matchers with ScalatestRouteTest with ScalaFutures with SprayJsonSupport
                  with BeforeAndAfterAll with BeforeAndAfterEach with JsonMarshalling with RestApiJsonProtocol
                  with GivenWhenThen
{
  implicit val _system = ActorSystem("TestSystem")
  val demander1 = "terry"
  val demander2 = "babs"
  val supplier1 = "willem"
  val supplier2 = "henk"
  val supplier3 = "piet"

  /** A static password, authentication is propely tested elsewhere. */
  val password = "1234"

  feature("The internal service must be exposed over the rest api") {
    scenario("1.") {
      val api = new RestApi(system, ApiModule(system))

      When("Users are created")
      Post("/api/v1/user", RestApi.Requests.CreateUser(demander1, password, UserTypes.demander)) ~> api.route ~> check {
        val resp = responseAs[String]
        resp shouldBe RestApi.Responses.Message("User created").asJson
      }

      Post("/api/v1/user", RestApi.Requests.CreateUser(demander2, password, UserTypes.demander)) ~> api.route ~> check {
        val resp = responseAs[String]
        resp shouldBe RestApi.Responses.Message("User created").asJson
      }

      Post("/api/v1/user", RestApi.Requests.CreateUser(supplier1, password, UserTypes.supplier)) ~> api.route ~> check {
        val resp = responseAs[String]
        resp shouldBe RestApi.Responses.Message("User created").asJson
      }

      Post("/api/v1/user", RestApi.Requests.CreateUser(supplier2, password, UserTypes.supplier)) ~> api.route ~> check {
        val resp = responseAs[String]
        resp shouldBe RestApi.Responses.Message("User created").asJson
      }

      Post("/api/v1/user", RestApi.Requests.CreateUser(supplier3, password, UserTypes.supplier)) ~> api.route ~> check {
        val resp = responseAs[String]
        resp shouldBe RestApi.Responses.Message("User created").asJson
      }

      Then("They may login")
      Post("/api/v1/login", RestApi.Requests.CreateUser(demander1, password, UserTypes.demander)) ~> api.route ~> check {
        val resp = responseAs[String].asObject[SessionRepr]
      }

      Post("/api/v1/login", RestApi.Requests.CreateUser(demander2, password, UserTypes.demander)) ~> api.route ~> check {
        val resp = responseAs[String].asObject[SessionRepr]
      }

      Post("/api/v1/login", RestApi.Requests.CreateUser(supplier1, password, UserTypes.supplier)) ~> api.route ~> check {
        val resp = responseAs[String].asObject[SessionRepr]
      }

      Post("/api/v1/login", RestApi.Requests.CreateUser(supplier2, password, UserTypes.supplier)) ~> api.route ~> check {
        val resp = responseAs[String].asObject[SessionRepr]
      }

      Post("/api/v1/login", RestApi.Requests.CreateUser(supplier3, password, UserTypes.supplier)) ~> api.route ~> check {
        val resp = responseAs[String].asObject[SessionRepr]
      }
    }
  }
}
