package nl.bluesoft.ondemand.demand

import java.util.UUID

import akka.testkit.TestProbe
import nl.bluesoft.ondemand.common.event._
import nl.bluesoft.ondemand.common.representation.RepresentationTypes._
import nl.bluesoft.ondemand.common.representation.SupplyUserRepr
import nl.bluesoft.ondemand.demand.system.DemandManager
import nl.bluesoft.ondemand.suite.TestSuite

import scala.concurrent.duration._
import scala.language.postfixOps

class DemandModuleIntegrationTest extends TestSuite
{
  feature("The DemandWorker should handle a single DemandRequest.") {
    scenario("1. The first service request is acknowledged") {
      val demandGroupID: DemandGroupID = UUID.randomUUID().toString
      val manager = _system.actorOf(DemandManager.props(demandGroupID), s"DemandManager")
      val probe = TestProbe()

      When("demand is requested")
      val demand1 = createDemand(demander1)
      eventStream.publish(SupplyUserLoggedIn(supplier1))
      eventStream.publish(SupplyUserLoggedIn(supplier2))
      eventStream.publish(SupplyUserLoggedIn(supplier3))

      eventually {
        probe.send(manager, DemandManager.GetSuppliers)
        probe.expectMsgPF(1 second) {
          case suppliers:Iterable[SupplyUserRepr@unchecked] if suppliers.size == 3 ⇒
          case _ ⇒ fail()
        }
      }

      eventStream.publish(DemandRequested(demand1))
      eventStream.expectMsg(ServiceRequested(demand1, supplier1))

      eventStream.publish(ServiceResponseReceived(demand1.id, supplier1, acknowledged = true))
      eventStream.expectMsg(DemandRequestCompleted(demand1, supplier1))

      probe.send(manager, DemandManager.GetDemandRepr(demand1.id))
      probe.expectMsg(demand1.copy(attemptedSuppliers = Seq(supplier1.username), servicingSupplier = Some(supplier1)))
    }

    scenario("2. The last service request is acknowledged") {
      val demandGroupID: DemandGroupID = UUID.randomUUID().toString
      val manager = _system.actorOf(DemandManager.props(demandGroupID), s"DemandManager")
      val probe = TestProbe()

      val demand1 = createDemand(demander1)
      eventStream.publish(SupplyUserLoggedIn(supplier1))
      eventStream.publish(SupplyUserLoggedIn(supplier2))
      eventStream.publish(SupplyUserLoggedIn(supplier3))

      eventually {
        probe.send(manager, DemandManager.GetSuppliers)
        probe.expectMsgPF(1 second) {
          case suppliers:Iterable[SupplyUserRepr@unchecked] if suppliers.size == 3 ⇒
          case _ ⇒ fail()
        }
      }

      When("demand is requested")
      eventStream.publish(DemandRequested(demand1))

      eventStream.expectMsg(ServiceRequested(demand1, supplier1))
      eventStream.publish(ServiceResponseReceived(demand1.id, supplier1, acknowledged = false))

      eventStream.expectMsg(ServiceRequested(demand1, supplier2))
      eventStream.publish(ServiceResponseReceived(demand1.id, supplier2, acknowledged = false))

      eventStream.expectMsg(ServiceRequested(demand1, supplier3))
      eventStream.publish(ServiceResponseReceived(demand1.id, supplier3, acknowledged = true))

      eventStream.expectMsg(DemandRequestCompleted(demand1, supplier3))
      probe.send(manager, DemandManager.GetDemandRepr(demand1.id))
      probe.expectMsg(demand1.copy(attemptedSuppliers = Seq(supplier1.username,supplier2.username,supplier3.username), servicingSupplier = Some(supplier3)))
    }

    scenario("3. The service requests are not acknowledged") {
      val demandGroupID: DemandGroupID = UUID.randomUUID().toString
      val manager = _system.actorOf(DemandManager.props(demandGroupID), s"DemandManager")
      val probe = TestProbe()

      val demand1 = createDemand(demander1)
      eventStream.publish(SupplyUserLoggedIn(supplier1))
      eventStream.publish(SupplyUserLoggedIn(supplier2))
      eventStream.publish(SupplyUserLoggedIn(supplier3))

      eventually {
        probe.send(manager, DemandManager.GetSuppliers)
        probe.expectMsgPF(1 second) {
          case suppliers:Iterable[SupplyUserRepr@unchecked] if suppliers.size == 3 ⇒
          case _ ⇒ fail()
        }
      }

      When("demand is requested")
      eventStream.publish(DemandRequested(demand1))

      eventStream.expectMsg(ServiceRequested(demand1, supplier1))
      eventStream.publish(ServiceResponseReceived(demand1.id, supplier1, acknowledged = false))

      eventStream.expectMsg(ServiceRequested(demand1, supplier2))
      eventStream.publish(ServiceResponseReceived(demand1.id, supplier2, acknowledged = false))

      eventStream.expectMsg(ServiceRequested(demand1, supplier3))
      eventStream.publish(ServiceResponseReceived(demand1.id, supplier3, acknowledged = false))

      eventStream.expectMsg(DemandRequestFailed(demand1, "There are no more suppliers available."))
      probe.send(manager, DemandManager.GetDemandRepr(demand1.id))
      probe.expectMsg(demand1.copy(attemptedSuppliers = Seq(supplier1.username,supplier2.username,supplier3.username)))
    }

    scenario("4. Timeouts occur until failed demand request") {
      val demandGroupID: DemandGroupID = UUID.randomUUID().toString
      val manager = _system.actorOf(DemandManager.props(demandGroupID), s"DemandManager")
      val probe = TestProbe()

      val demand1 = createDemand(demander1)
      eventStream.publish(SupplyUserLoggedIn(supplier1))
      eventStream.publish(SupplyUserLoggedIn(supplier2))
      eventStream.publish(SupplyUserLoggedIn(supplier3))

      eventually {
        probe.send(manager, DemandManager.GetSuppliers)
        probe.expectMsgPF(1 second) {
          case suppliers:Iterable[SupplyUserRepr@unchecked] if suppliers.size == 3 ⇒
          case _ ⇒ fail()
        }
      }

      When("demand is requested")
      eventStream.publish(DemandRequested(demand1))

      And("Timeout occurs")
      eventStream.expectMsg(ServiceRequested(demand1, supplier1))
      Then("The request must be retracted")
      eventStream.expectMsg(ServiceRequestRetracted(demand1, supplier1))

      And("Timeout occurs")
      eventStream.expectMsg(ServiceRequested(demand1, supplier2))
      Then("The request must be retracted")
      eventStream.expectMsg(ServiceRequestRetracted(demand1, supplier2))

      And("Timeout occurs")
      eventStream.expectMsg(ServiceRequested(demand1, supplier3))
      Then("The request must be retracted")
      eventStream.expectMsg(ServiceRequestRetracted(demand1, supplier3))

      And("The demand request fails")
      eventStream.expectMsg(DemandRequestFailed(demand1, "There are no more suppliers available."))

      probe.send(manager, DemandManager.GetDemandRepr(demand1.id))
      probe.expectMsg(demand1.copy(attemptedSuppliers = Seq(supplier1.username,supplier2.username,supplier3.username)))
    }
  }
}
