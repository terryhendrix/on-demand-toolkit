package nl.bluesoft.ondemand.demand

import java.util.UUID

import akka.actor.{ActorRef, Props}
import akka.testkit.TestProbe
import nl.bluesoft.ondemand.common.event.{DemandRequested, ServiceResponseReceived, SupplyUserLoggedIn, SupplyUserLoggedOut}
import nl.bluesoft.ondemand.common.representation.RepresentationTypes._
import nl.bluesoft.ondemand.common.representation.{DemandRepr, SupplyUserRepr}
import nl.bluesoft.ondemand.demand.system.{DemandManager, DemandWorker}
import nl.bluesoft.ondemand.suite.TestSuite
import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

object TestDemandManager {
  def props(groupId: DemandGroupID) = Props(new TestDemandManager(groupId))
  var children = Map.empty[String, TestProbe]
  var demandRequests = Map.empty[String, DemandRepr]
}

class TestDemandManager(override val groupId:DemandGroupID) extends DemandManager with MockedSupplierResolving
{
  override def preStart() = {
    super.preStart()
    TestDemandManager.children = Map.empty
  }

  override def worker(demand: DemandRepr): ActorRef = {
    log.info(s"Spawning worker for ${demand.id}")
    TestDemandManager.demandRequests = TestDemandManager.demandRequests + (demand.id → demand)
    TestDemandManager.children.get(demand.id) match {
      case Some(probe) ⇒
        probe.ref
      case None ⇒
        val probe = TestProbe()(context.system)
        TestDemandManager.children = TestDemandManager.children + (demand.id → probe)
        probe.ref
    }
  }

  override def getDemandRepr(demandID:DemandID):Future[DemandRepr]  = Future.successful(TestDemandManager.demandRequests(demandID))
}

class DemandManagerTest extends TestSuite
{
  override def beforeEach() = {
    super.beforeEach()
    TestDemandManager.children = Map.empty
    TestDemandManager.demandRequests = Map.empty
  }

  feature("The DemandManager should manage the demands within a service group.") {
    scenario("1. It should not be able to handle demand without suppliers") {
      val demandGroupID:DemandGroupID = UUID.randomUUID().toString
      val manager = _system.actorOf(TestDemandManager.props(demandGroupID), s"DemandManager")
      Thread sleep 100  // Sleep is required so the manager has time to subscribe to the event stream

      When("demand is requested")
      val demandRequestedEvent = DemandRequested(createDemand(demander1, demandGroupID))
      eventStream.publish(demandRequestedEvent)

      Then("the manager should spawn children")
      eventually {
        TestDemandManager.children should not be empty
      }

      And("the created child should receive a HandleDemand message")
      val worker = TestDemandManager.children.head._2
      worker.expectMsg(DemandWorker.HandleDemand)
      
      When("the worker asks for a next supplier")
      worker.send(manager, DemandManager.NextSupplier(demandRequestedEvent.demand, demander1, Seq.empty))

      Then("there are no logged in suppliers.")
      worker.expectMsg(DemandWorker.NoMoreSuppliers)
    }

    scenario("2. It should be able to determine suppliers until they have all run out") {
      val demandGroupID:DemandGroupID = UUID.randomUUID().toString
      val manager = _system.actorOf(TestDemandManager.props(demandGroupID), s"DemandManager")
      val probe = TestProbe()

      When("several users are logged in")
      eventStream.publish(SupplyUserLoggedIn(supplier1))
      eventStream.publish(SupplyUserLoggedIn(supplier2))
      eventStream.publish(SupplyUserLoggedIn(supplier3))

      eventually {
        probe.send(manager, DemandManager.GetSuppliers)
        probe.expectMsgPF(1 second) {
          case suppliers:Iterable[SupplyUserRepr@unchecked] if suppliers.size == 3 ⇒
        }
      }

      And("a supply user logs out again")
      eventStream.publish(SupplyUserLoggedOut(supplier2))

      eventually {
        probe.send(manager, DemandManager.GetSuppliers)
        probe.expectMsgPF(1 second) {
          case suppliers:Iterable[SupplyUserRepr@unchecked] if suppliers.size == 2 ⇒
        }
      }

      And("a demand is requested")
      val demandRequestedEvent = DemandRequested(createDemand(demander1, demandGroupID))
      eventStream.publish(demandRequestedEvent)

      Then("the manager should spawn children")
      eventually {
        TestDemandManager.children should not be empty
      }

      And("the created child should receive a HandleDemand message")
      val worker = TestDemandManager.children.head._2
      worker.expectMsg(DemandWorker.HandleDemand)

      When("the worker asks for the next supplier")
      worker.send(manager, DemandManager.NextSupplier(demandRequestedEvent.demand, demander1, Seq.empty))

      Then("There should be an available user")
      worker.expectMsg(DemandWorker.HandleSupplier(supplier1))

      When("the worker asks for the next supplier")
      worker.send(manager, DemandManager.NextSupplier(demandRequestedEvent.demand, demander1, Seq(supplier1)))

      Then("There should be an available user")
      worker.expectMsg(DemandWorker.HandleSupplier(supplier3))

      When("the worker asks for a next supplier")
      worker.send(manager, DemandManager.NextSupplier(demandRequestedEvent.demand, demander1, Seq(supplier1, supplier3)))

      Then("there are no logged in suppliers left.")
      worker.expectMsg(DemandWorker.NoMoreSuppliers)
    }

    scenario("3. It must route ServiceResponseReceived to the correct worker.") {
      val demandGroupID:DemandGroupID = UUID.randomUUID().toString
      val manager = _system.actorOf(TestDemandManager.props(demandGroupID), s"DemandManager")
      val probe = TestProbe()

      When("several users are logged in")
      eventStream.publish(SupplyUserLoggedIn(supplier1))
      eventStream.publish(SupplyUserLoggedIn(supplier2))
      eventStream.publish(SupplyUserLoggedIn(supplier3))

      eventually {
        probe.send(manager, DemandManager.GetSuppliers)
        probe.expectMsgPF(1 second) {
          case suppliers:Iterable[SupplyUserRepr@unchecked] if suppliers.size == 3 ⇒
        }
      }

      val demand1 = createDemand(demander1)
      val demand2 = createDemand(demander1)
      val demand3 = createDemand(demander1)

      And("demand requests are made")
      eventStream.publish(DemandRequested(demand1))
      eventually {
        TestDemandManager.children.size shouldBe 1
      }

      eventStream.publish(DemandRequested(demand2))
      eventually {
        TestDemandManager.children.size shouldBe 2
      }

      eventStream.publish(DemandRequested(demand3))
      eventually {
        TestDemandManager.children.size shouldBe 3
      }

      And("the created children should receive a HandleDemand message")
      TestDemandManager.children.foreach(e ⇒ e._2.expectMsg(DemandWorker.HandleDemand))

      Then("The requested supplier replies to the service request")
      val worker1 = TestDemandManager.children.head
      eventStream.publish(ServiceResponseReceived(demand1.id, supplier1, acknowledged = true))
      worker1._2.expectMsg(ServiceResponseReceived(demand1.id, supplier1, acknowledged = true))

      val worker2 = TestDemandManager.children.tail.head
      eventStream.publish(ServiceResponseReceived(demand2.id, supplier1, acknowledged = true))
      worker2._2.expectMsg(ServiceResponseReceived(demand2.id, supplier1, acknowledged = true))

      val worker3 = TestDemandManager.children.tail.tail.head
      eventStream.publish(ServiceResponseReceived(demand3.id, supplier1, acknowledged = true))
      worker3._2.expectMsg(ServiceResponseReceived(demand3.id, supplier1, acknowledged = true))
    }
  }
}
