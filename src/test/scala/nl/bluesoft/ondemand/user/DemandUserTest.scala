package nl.bluesoft.ondemand.user

import akka.actor.Props
import akka.testkit.TestProbe
import nl.bluesoft.ondemand.common.event._
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.UserTypes
import nl.bluesoft.ondemand.suite.TestSuite
import nl.bluesoft.ondemand.user.system.{DemandUser, User, UserManager}

import scala.language.postfixOps

object TestDemandUser {
  sealed class TestDemandUser extends DemandUser with DemandUserEventStreamPublishing with MockedUserManagement with EventStreamPushNotifications
}

class DemandUserTest extends TestSuite
{
  import TestDemandUser._

  feature("A demand user symbolize an end user in the role of demander.") {
    scenario("1. A demand user may create an account on the system") {
      val user1 = _system.actorOf(Props(new TestDemandUser), s"DemandUser1")
      val user2 = _system.actorOf(Props(new TestDemandUser), s"DemandUser2")
      val user3 = _system.actorOf(Props(new TestDemandUser), s"DemandUser3")

      val manager = TestProbe()

      When("demand users are created")
      manager.send(user1, User.CreateUser(demander1.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(demander1))

      manager.send(user2, User.CreateUser(demander2.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(demander2))

      manager.send(user3, User.CreateUser(demander3.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(demander3))

      manager.expectNoMsg(expectNoMessageDuration)

      manager.send(user1, User.CreateUser(demander1.username, "1234"))
      manager.expectMsg(User.UserAlreadyExists(demander1.username))

      manager.send(user2, User.CreateUser(demander2.username, "1234"))
      manager.expectMsg(User.UserAlreadyExists(demander2.username))

      manager.send(user3, User.CreateUser(demander3.username, "1234"))
      manager.expectMsg(User.UserAlreadyExists(demander3.username))

      Then("messages about the creation must be published")
      eventStream.expectMsgAllOf(DemandUserCreated(demander1),DemandUserCreated(demander2),DemandUserCreated(demander3))
      eventStream.expectNoMsg(expectNoMessageDuration)
    }

    scenario("2. A demand user may demand something and the demand can be supplied.") {
      val user1 = _system.actorOf(Props(new TestDemandUser), s"DemandUser1")
      val manager = TestProbe()

      manager.send(user1, User.CreateUser(demander1.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(demander1))
      manager.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsgAllOf(DemandUserCreated(demander1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      When("a demand user has requested demand")
      val demand1 = createDemand(demander1)
      manager.send(user1, DemandUser.RequestDemand(demand1))
      manager.expectNoMsg(expectNoMessageDuration)
      eventStream.expectMsg(DemandRequested(demand1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      And("a supply user acknowledges the demand")
      manager.send(user1, DemandRequestCompleted(demand1, supplier1))
      manager.expectNoMsg(expectNoMessageDuration)

      Then("the demand user must receive a push notification.")
      eventStream.expectMsg(PushNotificationReceived(demander1, "DemandRequestCompleted", DemandRequestCompleted(demand1, supplier1)))
    }

    scenario("3. A demand user may demand something and but there is no supply.") {
      val user1 = _system.actorOf(Props(new TestDemandUser), s"DemandUser1")
      val manager = TestProbe()

      manager.send(user1, User.CreateUser(demander1.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(demander1))
      manager.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsgAllOf(DemandUserCreated(demander1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      When("a demand user has requested demand.")
      val demand1 = createDemand(demander1)
      manager.send(user1, DemandUser.RequestDemand(demand1))
      manager.expectNoMsg(expectNoMessageDuration)
      eventStream.expectMsg(DemandRequested(demand1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      And("there is no supply.")
      manager.send(user1, DemandRequestFailed(demand1, "There is no supply"))
      manager.expectNoMsg(expectNoMessageDuration)

      Then("the demand user must receive a push notification that the request failed.")
      eventStream.expectMsg(PushNotificationReceived(demander1, "DemandRequestFailed", DemandRequestFailed(demand1, "There is no supply")))
    }

    scenario("4. Authentication may be successful.") {
      val user1 = _system.actorOf(Props(new TestDemandUser), s"DemandUser1")
      val manager = TestProbe()

      manager.send(user1, User.CreateUser(demander1.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(demander1))
      manager.expectNoMsg(expectNoMessageDuration)
      eventStream.expectMsgAllOf(DemandUserCreated(demander1))

      When("a user attempts to authenticate with the correct credentials.")
      manager.send(user1, User.Authenticate(demander1.username, "1234", UserTypes.demander))
      Then("the user is authenticated.")
      manager.expectMsg(User.Authenticated(demander1.username))

      eventStream.expectNoMsg(expectNoMessageDuration)
    }

    scenario("5. Authentication may fail.") {
      val user1 = _system.actorOf(Props(new TestDemandUser), s"DemandUser1")
      val manager = TestProbe()

      manager.send(user1, User.CreateUser(demander1.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(demander1))
      manager.expectNoMsg(expectNoMessageDuration)
      eventStream.expectMsgAllOf(DemandUserCreated(demander1))

      When("a user attempts to authenticate with incorrect credentials.")
      manager.send(user1, User.Authenticate(demander1.username, "12345", UserTypes.demander))
      Then("the the authentication fails..")
      manager.expectMsg(FalseLoginAttempted(demander1.username))

      And("a message is placed on the event stream indicating an incorrect login attempt.")
      eventStream.expectMsg(FalseLoginAttempted(demander1.username))
      eventStream.expectNoMsg(expectNoMessageDuration)
    }
  }
}
