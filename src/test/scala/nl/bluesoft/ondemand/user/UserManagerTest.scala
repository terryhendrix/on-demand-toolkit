package nl.bluesoft.ondemand.user
import akka.actor.Props
import akka.testkit.TestProbe
import nl.bluesoft.ondemand.common.event._
import nl.bluesoft.ondemand.common.representation.RepresentationTypes.UserTypes
import nl.bluesoft.ondemand.suite.TestSuite
import nl.bluesoft.ondemand.user.system.{User, UserManager}

import scala.language.postfixOps

object UserManagerTest
{
  def managerProps = Props(new TestUserManager)

  sealed class TestUserManager extends UserManager
}

class UserManagerTest extends TestSuite
{
  import UserModuleTest._

  override def beforeEach() = {
    super.beforeEach()
  }

  feature("user accounts should be managed and receive message from the event stream") {
    scenario("1. As a demand user, i must create a user account.") {
      val manager = _system.actorOf(managerProps, "UserManager")
      val probe = TestProbe()

      probe.send(manager, UserManager.Register(demander1.username, "1234", UserTypes.demander))
      probe.expectMsg(UserManager.UserCreated(demander1))
      probe.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsg(DemandUserCreated(demander1))
      eventStream.expectNoMsg(expectNoMessageDuration)
    }

    scenario("2. As a demand user, i must be able to be authenticated.") {
      val manager = _system.actorOf(managerProps, "UserManager")
      val probe = TestProbe()

      probe.send(manager, UserManager.Register(demander1.username, "1234", UserTypes.demander))
      probe.expectMsg(UserManager.UserCreated(demander1))
      probe.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsg(DemandUserCreated(demander1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      probe.send(manager, UserManager.Login(demander1.username, "1234", UserTypes.demander))
      probe.expectMsg(User.Authenticated(demander1.username))
      probe.expectNoMsg(expectNoMessageDuration)
    }

    scenario("3. As a demand user, my account should be protected from incorrect login attempts.") {
      val manager = _system.actorOf(managerProps, "UserManager")
      val probe = TestProbe()

      probe.send(manager, UserManager.Register(demander1.username, "1234", UserTypes.demander))
      probe.expectMsg(UserManager.UserCreated(demander1))
      eventStream.expectMsg(DemandUserCreated(demander1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      probe.send(manager, UserManager.Login(demander1.username, "12345", UserTypes.demander))
      probe.expectMsg(FalseLoginAttempted(demander1.username))
      probe.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsg(FalseLoginAttempted(demander1.username))
      eventStream.expectNoMsg(expectNoMessageDuration)
    }

    scenario("4. As a supply user, i must create a user account.") {
      val manager = _system.actorOf(managerProps, "UserManager")
      val probe = TestProbe()

      probe.send(manager, UserManager.Register(supplier1.username, "1234", UserTypes.supplier))
      probe.expectMsg(UserManager.UserCreated(supplier1))
      probe.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsg(SupplyUserCreated(supplier1))
      eventStream.expectNoMsg(expectNoMessageDuration)
    }

    scenario("5. As a supply user, i must be able to be authenticated.") {
      val manager = _system.actorOf(managerProps, "UserManager")
      val probe = TestProbe()

      probe.send(manager, UserManager.Register(supplier1.username, "1234", UserTypes.supplier))
      probe.expectMsg(UserManager.UserCreated(supplier1))
      probe.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsg(SupplyUserCreated(supplier1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      probe.send(manager, UserManager.Login(supplier1.username, "1234", UserTypes.supplier))
      probe.expectMsg(User.Authenticated(supplier1.username))
      probe.expectNoMsg(expectNoMessageDuration)
    }

    scenario("6. As a supply user, my account should be protected from incorrect login attempts.") {
      val manager = _system.actorOf(managerProps, "UserManager")
      val probe = TestProbe()

      probe.send(manager, UserManager.Register(supplier1.username, "1234", UserTypes.supplier))
      probe.expectMsg(UserManager.UserCreated(supplier1))
      eventStream.expectMsg(SupplyUserCreated(supplier1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      probe.send(manager, UserManager.Login(supplier1.username, "12345", UserTypes.supplier))
      probe.expectMsg(FalseLoginAttempted(supplier1.username))
      probe.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsg(FalseLoginAttempted(supplier1.username))
      eventStream.expectNoMsg(expectNoMessageDuration)
    }

    scenario("7. As a supply user, i must receive notifications about service requests") {
      val manager = _system.actorOf(managerProps, "UserManager")
      val probe = TestProbe()

      probe.send(manager, UserManager.Register(supplier1.username, "1234", UserTypes.supplier))
      probe.expectMsg(UserManager.UserCreated(supplier1))
      eventStream.expectMsg(SupplyUserCreated(supplier1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      val demand1 = createDemand(demander1)
      probe.send(manager, ServiceRequested(demand1, supplier1))
      probe.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsg(PushNotificationReceived(supplier1, "ServiceRequested", ServiceRequested(demand1, supplier1)))
      eventStream.expectNoMsg(expectNoMessageDuration)
    }

    scenario("8. As a supply user, i must receive notifications about service request retractions") {
      val manager = _system.actorOf(managerProps, "UserManager")
      val probe = TestProbe()

      probe.send(manager, UserManager.Register(supplier1.username, "1234", UserTypes.supplier))
      probe.expectMsg(UserManager.UserCreated(supplier1))
      eventStream.expectMsg(SupplyUserCreated(supplier1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      val demand1 = createDemand(demander1)
      probe.send(manager, ServiceRequestRetracted(demand1, supplier1))
      probe.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsg(PushNotificationReceived(supplier1, "ServiceRequestRetracted", ServiceRequestRetracted(demand1, supplier1)))
      eventStream.expectNoMsg(expectNoMessageDuration)
    }

    scenario("9. As a demand user, i must receive notifications about completed demand requests") {
      val manager = _system.actorOf(managerProps, "UserManager")
      val probe = TestProbe()

      probe.send(manager, UserManager.Register(demander1.username, "1234", UserTypes.demander))
      probe.expectMsg(UserManager.UserCreated(demander1))
      eventStream.expectMsg(DemandUserCreated(demander1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      val demand1 = createDemand(demander1)
      probe.send(manager, DemandRequestCompleted(demand1, supplier1))
      probe.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsg(PushNotificationReceived(demander1, "DemandRequestCompleted", DemandRequestCompleted(demand1, supplier1)))
      eventStream.expectNoMsg(expectNoMessageDuration)
    }

    scenario("10. As a demand user, i must receive notifications about failed demand requests") {
      val manager = _system.actorOf(managerProps, "UserManager")
      val probe = TestProbe()

      probe.send(manager, UserManager.Register(demander1.username, "1234", UserTypes.demander))
      probe.expectMsg(UserManager.UserCreated(demander1))
      eventStream.expectMsg(DemandUserCreated(demander1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      val demand1 = createDemand(demander1)
      probe.send(manager, DemandRequestFailed(demand1, "There are no more suppliers."))
      probe.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsg(PushNotificationReceived(demander1, "DemandRequestFailed", DemandRequestFailed(demand1, "There are no more suppliers.")))
      eventStream.expectNoMsg(expectNoMessageDuration)
    }
  }
}

