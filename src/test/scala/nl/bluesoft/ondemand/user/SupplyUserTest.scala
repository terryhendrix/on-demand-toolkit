package nl.bluesoft.ondemand.user

import akka.actor.Props
import akka.testkit.TestProbe
import nl.bluesoft.ondemand.common.event.{PushNotificationReceived, ServiceRequested, ServiceResponseReceived, SupplyUserCreated}
import nl.bluesoft.ondemand.suite.TestSuite
import nl.bluesoft.ondemand.user.system.{SupplyUser, User, UserManager}

import scala.language.postfixOps

object SupplyUserTest {
  sealed class TestSupplyUser extends SupplyUser with SupplyUserEventStreamPublishing with MockedUserManagement with EventStreamPushNotifications
}

class SupplyUserTest extends TestSuite
{
  import SupplyUserTest._

  feature("A supply user should be able to communicate with the outside world and store data about it's user.") {
    scenario("1. A demand user may create an account on the system") {
      val user1 = _system.actorOf(Props(new TestSupplyUser), s"SupplyUser1")
      val user2 = _system.actorOf(Props(new TestSupplyUser), s"SupplyUser2")
      val user3 = _system.actorOf(Props(new TestSupplyUser), s"SupplyUser3")

      val manager = TestProbe()

      When("supply users are created.")
      manager.send(user1, User.CreateUser(supplier1.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(supplier1))

      manager.send(user2, User.CreateUser(supplier2.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(supplier2))

      manager.send(user3, User.CreateUser(supplier3.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(supplier3))

      manager.expectNoMsg(expectNoMessageDuration)

      manager.send(user1, User.CreateUser(supplier1.username, "1234"))
      manager.expectMsg(User.UserAlreadyExists(supplier1.username))

      manager.send(user1, User.CreateUser(supplier2.username, "1234"))
      manager.expectMsg(User.UserAlreadyExists(supplier2.username))

      manager.send(user1, User.CreateUser(supplier3.username, "1234"))
      manager.expectMsg(User.UserAlreadyExists(supplier3.username))
      
      Then("messages about the creation must be published.")
      eventStream.expectMsgAllOf(SupplyUserCreated(supplier1), SupplyUserCreated(supplier2), SupplyUserCreated(supplier3))
      eventStream.expectNoMsg(expectNoMessageDuration)
    }

    scenario("2. A supply user may acknowledge service requests when they arrive") {
      val user1 = _system.actorOf(Props(new TestSupplyUser), s"SupplyUser1")
      val manager = TestProbe()

      manager.send(user1, User.CreateUser(supplier1.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(supplier1))
      manager.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsgAllOf(SupplyUserCreated(supplier1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      When("service is requested from the supplier.")
      val demand1 = createDemand(demander1)
      manager.send(user1, ServiceRequested(demand1, supplier1))

      Then("the supplier will get a push notification about this.")
      eventStream.expectMsg(PushNotificationReceived(supplier1, "ServiceRequested", ServiceRequested(demand1, supplier1)))

      When("the supplier acknowledges the request")
      manager.send(user1, SupplyUser.RespondToServiceRequest(demand1.id, supplier1.username, acknowledged = true))

      Then("a service response event will occur")
      eventStream.expectMsg(ServiceResponseReceived(demand1.id, supplier1, acknowledged = true))
    }

    scenario("2. A supply user decline service requests when they arrive") {
      val user1 = _system.actorOf(Props(new TestSupplyUser), s"SupplyUser1")
      val manager = TestProbe()

      manager.send(user1, User.CreateUser(supplier1.username, "1234"))
      manager.expectMsg(UserManager.UserCreated(supplier1))
      manager.expectNoMsg(expectNoMessageDuration)

      eventStream.expectMsgAllOf(SupplyUserCreated(supplier1))
      eventStream.expectNoMsg(expectNoMessageDuration)

      When("service is requested from the supplier.")
      val demand1 = createDemand(demander1)
      manager.send(user1, ServiceRequested(demand1, supplier1))

      Then("the supplier will get a push notification about this.")
      eventStream.expectMsg(PushNotificationReceived(supplier1, "ServiceRequested", ServiceRequested(demand1, supplier1)))

      When("the supplier declines the request")
      manager.send(user1, SupplyUser.RespondToServiceRequest(demand1.id, supplier1.username, acknowledged = false))

      Then("a service response event will occur")
      eventStream.expectMsg(ServiceResponseReceived(demand1.id, supplier1, acknowledged = false))
    }
  }
}
