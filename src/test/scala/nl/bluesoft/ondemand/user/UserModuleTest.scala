package nl.bluesoft.ondemand.user
import akka.actor.Props
import nl.bluesoft.ondemand.common.representation.RepresentationTypes
import RepresentationTypes.UserTypes
import nl.bluesoft.ondemand.suite.TestSuite
import nl.bluesoft.ondemand.user.system.UserManager

import scala.concurrent.duration._
import scala.language.postfixOps

object UserModuleTest
{
  def managerProps = Props(new TestUserManager)

  sealed class TestUserManager extends UserManager
}

class UserModuleTest extends TestSuite
{

  override def beforeEach() = {
    super.beforeEach()
  }

  feature("The UserModule should hide internal complexity") {
    scenario("1. As a demand user, i must create a user account.") {
      val users = UserModule(_system)
      users.registerUser("terry", "1234", UserTypes.demander).futureValue shouldBe true
      users.registerUser("terry", "1234", UserTypes.demander).futureValue shouldBe false
    }

    scenario("2. As a demand user, i must be able to be authenticated.") {
      val users = UserModule(_system)
      users.registerUser("terry", "1234", UserTypes.demander).futureValue shouldBe true
      users.loginUser("terry", "1234", UserTypes.demander).futureValue shouldBe true
    }

    scenario("3. As a demand user, my account should be protected from incorrect login attempts.") {
      val users = UserModule(_system)
      users.registerUser("terry", "1234", UserTypes.demander).futureValue shouldBe true
      users.loginUser("terry", "12345", UserTypes.demander).futureValue shouldBe false
    }

    scenario("4. As a supply user, i must create a user account.") {
      val users = UserModule(_system)
      users.registerUser("terry", "1234", UserTypes.supplier).futureValue shouldBe true
      users.registerUser("terry", "1234", UserTypes.supplier).futureValue shouldBe false
    }

    scenario("5. As a supply user, i must be able to be authenticated.") {
      val users = UserModule(_system)
      users.registerUser("terry", "1234", UserTypes.supplier).futureValue shouldBe true
      users.loginUser("terry", "1234", UserTypes.supplier).futureValue shouldBe true
    }

    scenario("6. As a supply user, my account should be protected from incorrect login attempts.") {
      val users = UserModule(_system)
      users.registerUser("terry", "1234", UserTypes.supplier).futureValue shouldBe true
      users.loginUser("terry", "12345", UserTypes.supplier).futureValue shouldBe false
    }

    scenario("7. As a service manager, i dont want peoply without accounts logging in.") {
      val users = UserModule(_system)
      users.loginUser("terry", "1234", UserTypes.demander).futureValue shouldBe false
      users.loginUser("terry", "1234", UserTypes.supplier).futureValue shouldBe false
    }
  }
}
